# Contributor: Kiyoshi Aman <kiyoshi.aman@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=python3
pkgver=3.6.5
_basever="${pkgver%.*}"
pkgrel=0
pkgdesc="A high-level scripting language"
url="https://www.python.org"
arch="all"
license="Python-2.0"
provides="py3-pip"
subpackages="$pkgname-dbg $pkgname-dev $pkgname-doc $pkgname-tests::noarch
	$pkgname-wininst"
# If Python 3 is installed during the build, you get:
#
# Installing collected packages: setuptools, pip
#   Found existing installation: setuptools 28.8.0
#     Uninstalling setuptools-28.8.0:
# Exception:
# Traceback (most recent call last):
#   File "/usr/src/packages/system/python3/src/Python-3.6.5/Lib/shutil.py", line 544, in move
#     os.rename(src, real_dst)
# PermissionError: [Errno 13] Permission denied: '/usr/bin/easy_install-3.6' -> '/tmp/pip-_n3b1tzj-uninstall/usr/bin/easy_install-3.6'
#
# and then 'setuptools' is not installed in the package image.
#
# This breaks a great number of things, including the ability to build most
# py3-* packages from abuild.
#
# Do not, under any circumstance, remove "!python3" from makedepends.
#
# I repeat.
#
# DO NOT REMOVE "!python3" FROM MAKEDEPENDS.
#
# This has been a message from A. Wilcox and the Foundation for Ensuring
# Packages Are Actually Buildable.
makedepends="expat-dev openssl-dev zlib-dev ncurses-dev bzip2-dev xz-dev
	sqlite-dev libffi-dev tcl-dev linux-headers !python3"
source="https://www.python.org/ftp/python/$pkgver/Python-$pkgver.tar.xz
	musl-find_library.patch
	fix-xattrs-glibc.patch
	"
builddir="$srcdir/Python-$pkgver"

prepare() {
	default_prepare

	# force system libs
	rm -r Modules/expat \
		Modules/zlib \
		Modules/_ctypes/darwin* \
		Modules/_ctypes/libffi*
}

build() {
	# --enable-optimizations is not enabled because it
	# is very, very slow as many tests are ran sequentially
	# for profile guided optimizations. additionally it
	# seems some of the training tests hang on certain
	# e.g. architectures (x86) possibly due to grsec or musl.

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-ipv6 \
		--enable-loadable-sqlite-extensions \
		--enable-shared \
		--with-lto \
		--with-computed-gotos \
		--with-dbmliborder=ndbm \
		--with-system-expat \
		--with-system-ffi \
		--with-threads

	# set thread stack size to 1MB so we don't segfault before we hit
	# sys.getrecursionlimit()
	make EXTRA_CFLAGS="$CFLAGS -DTHREAD_STACK_SIZE=0x100000"
}

check() {
	# test that we reach recursionlimit before we segfault
	cat > test-stacksize.py <<-EOF
	import threading
	import sys

	def fun(i):
	  try:
	    fun(i+1)
	  except:
	    sys.exit(0)

	t = threading.Thread(target=fun, args=[1])
	t.start()
EOF
	LD_LIBRARY_PATH=$PWD ./python test-stacksize.py

	local fail

	# musl related
	fail="test__locale test_locale test_strptime test_re"	# various musl locale deficiencies
	fail="$fail test_datetime"				# hangs if 'tzdata' installed
	fail="$fail test_os"					# fpathconf, ttyname errno values
	fail="$fail test_posix"					# sched_[gs]etscheduler not impl
	fail="$fail test_shutil"				# lchmod, requires real unzip

	# failures needing investigation
	fail="$fail test_faulthandler test_gdb"			# hangs(?)
	fail="$fail test_tokenize test_tools"			# SLOW (~60s)
	fail="$fail test_capi"					# test.test_capi.EmbeddingTests
	fail="$fail test_threadsignals"				# test_{,r}lock_acquire_interruption
	fail="$fail test_time"					# strftime/strptime %Z related
	fail="$fail test_cmath test_math"			# hang(?) on x86
	fail="$fail test_hash test_plistlib"			# fail on armhf
	fail="$fail test_ctypes"				# fail on aarch64 (ctypes.test.test_win32.Structures)

	# kernel related
	fail="$fail test_fcntl"					# wants DNOTIFY, we don't have it

	# dumb
	fail="$fail test_ssl"					# tries to do SSLv2 which we have disabled in OpenSSL

	make quicktest TESTOPTS="--exclude $fail"
}

package() {
	make -j1 DESTDIR="$pkgdir" EXTRA_CFLAGS="$CFLAGS" install maninstall
	install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
	# those are provided by python3-tkinter
	rm -r "$pkgdir"/usr/bin/idle* "$pkgdir"/usr/lib/python*/idlelib \
		"$pkgdir"/usr/lib/python*/tkinter
}

dev() {
	default_dev

	# pyconfig.h is needed runtime so we move it back
	mkdir -p "$pkgdir"/usr/include/python${_basever}m
	mv "$subpkgdir"/usr/include/python${_basever}m/pyconfig.h \
		"$pkgdir"/usr/include/python${_basever}m/
}

tests() {
	pkgdesc="Test modules from the main Python package"

	cd "$pkgdir"/usr/lib/python$_basever
	local i; for i in */test */tests; do
		mkdir -p "$subpkgdir"/usr/lib/python$_basever/"$i"
		mv "$i"/* "$subpkgdir"/usr/lib/python$_basever/"$i"
		rm -rf "$i"
	done
	mv "$pkgdir"/usr/lib/python$_basever/test \
		"$subpkgdir"/usr/lib/python$_basever/
}

wininst() {
	pkgdesc="Python wininst files"
	mkdir -p "$subpkgdir"/usr/lib/python$_basever/distutils/command
	mv "$pkgdir"/usr/lib/python$_basever/distutils/command/*.exe \
		"$subpkgdir"/usr/lib/python$_basever/distutils/command
}

sha512sums="6b26fcd296b9bd8e67861eff10d14db7507711ddba947288d16d6def53135c39326b7f969c04bb2b2993f924d9e7ad3f5c5282a3915760bc0885cf0a8ea5eb51  Python-3.6.5.tar.xz
ab8eaa2858d5109049b1f9f553198d40e0ef8d78211ad6455f7b491af525bffb16738fed60fc84e960c4889568d25753b9e4a1494834fea48291b33f07000ec2  musl-find_library.patch
37b6ee5d0d5de43799316aa111423ba5a666c17dc7f81b04c330f59c1d1565540eac4c585abe2199bbed52ebe7426001edb1c53bd0a17486a2a8e052d0f494ad  fix-xattrs-glibc.patch"
