# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Horst Burkhardt <horst@adelielinux.org>
pkgname=openssh
pkgver=7.9_p1
_myver=${pkgver%_*}${pkgver#*_}
pkgrel=3
pkgdesc="Port of OpenBSD's free SSH release"
url="https://www.openssh.com/portable.html"
arch="all"
license="BSD-1-Clause AND BSD-2-Clause AND BSD-3-Clause"
options="suid !check"
depends="openssh-client openssh-sftp-server openssh-server"
makedepends_build="linux-pam-dev gettext-tiny"
makedepends_host="openssl-dev zlib-dev linux-headers linux-pam-dev
	gettext-tiny-dev utmps-dev"
makedepends="$makedepends_build $makedepends_host"
subpackages="$pkgname-doc
	$pkgname-keygen
	$pkgname-client
	$pkgname-keysign
	$pkgname-sftp-server:sftp
	$pkgname-server
	$pkgname-openrc
	"

source="https://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/$pkgname-$_myver.tar.gz
	bsd-compatible-realpath.patch
	CVE-2018-20685.patch
	disable-forwarding-by-default.patch
	fix-utmpx.patch
	openssh7.4-peaktput.patch
	openssh-7.9_p1-openssl-1.0.2-compat.patch
	sftp-interactive.patch

	sshd.initd
	sshd.confd
	"
# secfixes:
#   7.9_p1-r2:
#     - CVE-2018-20685
#   7.9_p1:
#     - CVE-2018-15473
#   7.7_p1:
#     - CVE-2017-15906
#   7.4_p1:
#     - CVE-2016-10009
#     - CVE-2016-10010
#     - CVE-2016-10011
#     - CVE-2016-10012

# HPN patches are from: http://hpnssh.sourceforge.net/

builddir="$srcdir"/$pkgname-$_myver

build() {
	cd "$builddir"
	export LD="$CC"
	LIBS="-lutmps -lskarnet" ./configure --build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc/ssh \
		--libexecdir=/usr/lib/ssh \
		--mandir=/usr/share/man \
		--with-pid-dir=/run \
		--with-mantype=man \
		--with-ldflags="${LDFLAGS}" \
		--enable-lastlog \
		--disable-strip \
		--enable-wtmp \
		--with-privsep-path=/var/empty \
		--with-xauth=/usr/bin/xauth \
		--with-privsep-user=sshd \
		--with-md5-passwords \
		--with-ssl-engine \
		--with-pam
	make
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
	mkdir -p "$pkgdir"/var/empty
	install -D -m755 "$srcdir"/sshd.initd \
		"$pkgdir"/etc/init.d/sshd
	install -D -m644 "$srcdir"/sshd.confd \
		"$pkgdir"/etc/conf.d/sshd
	install -Dm644 "$builddir"/contrib/ssh-copy-id.1 \
		"$pkgdir"/usr/share/man/man1/ssh-copy-id.1
}

keygen() {
	pkgdesc="Helper program for generating SSH keys"
	depends=
	install -d "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/ssh-keygen \
		"$subpkgdir"/usr/bin/
}

client() {
	pkgdesc="OpenBSD's SSH client"
	depends="openssh-keygen"
	install -d "$subpkgdir"/usr/bin \
		"$subpkgdir"/usr/lib/ssh \
		"$subpkgdir"/etc/ssh \
		"$subpkgdir"/var/empty

	mv "$pkgdir"/usr/bin/* \
		"$subpkgdir"/usr/bin/
	mv "$pkgdir"/etc/ssh/ssh_config \
		"$pkgdir"/etc/ssh/moduli \
		"$subpkgdir"/etc/ssh/
	install -Dm755 "$builddir"/contrib/findssl.sh \
		"$subpkgdir"/usr/bin/findssl.sh
	install -Dm755 "$builddir"/contrib/ssh-copy-id \
		"$subpkgdir"/usr/bin/ssh-copy-id
	install -Dm755	"$builddir"/ssh-pkcs11-helper \
		"$subpkgdir"/usr/bin/ssh-pkcs11-helper
}

keysign() {
	pkgdesc="Helper program for SSH host-based authentication"
	depends="openssh-client"
	install -d "$subpkgdir"/usr/lib/ssh
	mv "$pkgdir"/usr/lib/ssh/ssh-keysign \
		"$subpkgdir"/usr/lib/ssh/
}

sftp() {
	pkgdesc="SFTP server module for OpenSSH"
	depends=""
	install -d "$subpkgdir"/usr/lib/ssh
	mv "$pkgdir"/usr/lib/ssh/sftp-server \
		"$subpkgdir"/usr/lib/ssh/
}

server() {
	pkgdesc="OpenSSH server"
	depends="openssh-keygen"
	replaces="openssh-server-common"
	cd "$builddir"
	install -d "$subpkgdir"/usr/sbin
	install -d "$subpkgdir"/etc/ssh
	mv "$pkgdir"/usr/sbin/sshd "$subpkgdir"/usr/sbin/
	mv "$pkgdir"/etc/ssh/sshd_config "$subpkgdir"/etc/ssh/
}

openrc() {
	default_openrc
	depends="openssh-server"
	install_if="openssh-server=$pkgver-r$pkgrel openrc"
}

sha512sums="0412c9c429c9287f0794023951469c8e6ec833cdb55821bfa0300dd90d0879ff60484f620cffd93372641ab69bf0b032c2d700ccc680950892725fb631b7708e  openssh-7.9p1.tar.gz
f2b8daa537ea3f32754a4485492cc6eb3f40133ed46c0a5a29a89e4bcf8583d82d891d94bf2e5eb1c916fa68ec094abf4e6cd641e9737a6c05053808012b3a73  bsd-compatible-realpath.patch
b8907d3d6ebceeca15f6bc97551a7613c68df5c31e4e76d43b7c0bd9ad42dedcabc20a2cc5404b89f40850a4765b24892bde50eab1db55c96ad5cf23bb1f8d04  CVE-2018-20685.patch
f3d5960572ddf49635d4edbdff45835df1b538a81840db169c36b39862e6fa8b0393ca90626000b758f59567ff6810b2537304098652483b3b31fb438a061de6  disable-forwarding-by-default.patch
0c1e832cec420bc7b57558041d2288912a438db97050b87f6a57e94a2741a374cc5d141fe352968b0d1ba6accaff965794463fe9169d136678a8915a60d2f0b7  fix-utmpx.patch
398096a89aa104abeff31aa043ac406a6348e0fdd4d313b7888ee0b931d38fd71fc21bceee46145e88f03bc27e00890e068442faee2d33f86cfbc04d58ffa4b6  openssh7.4-peaktput.patch
dde28496df7ee74a2bbcf0aba389abefade3dc41f7d10dc6d3c1a0aca087478bafe10d31ec5e61e758084fa0a2a7c64314502091d900d9cee487c1bdc92722a6  openssh-7.9_p1-openssl-1.0.2-compat.patch
c1d09c65dbc347f0904edc30f91aa9a24b0baee50309536182455b544f1e3f85a8cecfa959e32be8b101d8282ef06dde3febbbc3f315489339dcf04155c859a9  sftp-interactive.patch
394a420a36880bb0dd37dfd8727cea91fd9de6534050169e21212a46513ef3aaafe2752c338699b3d4ccd14871b26cf01a152df8060cd37f86ce0665fd53c63f  sshd.initd
ce0abddbd2004891f88efd8522c4b37a4989290269fab339c0fa9aacc051f7fd3b20813e192e92e0e64315750041cb74012d4321260f4865ff69d7a935b259d4  sshd.confd"
