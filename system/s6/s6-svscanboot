#!/bin/execlineb -P

# Basic sanity.

export PATH /usr/bin:/usr/sbin:/bin:/sbin
umask 022


# The live service directories are in /run/services
# The scandir is /run/service, containing only the catch-all logger and symlinks
# (and the s6-svscan configuration in /run/service/.s6-svscan)

if { rm -rf /run/service /run/services }
if { mkdir -p -m 0755 /run/service/.s6-svscan /run/service/s6-svscan-log }
if
{
  redirfd -w 1 /run/service/.s6-svscan/crash
  heredoc 0 "#!/bin/execlineb -P\nfalse\n"
  cat
}
if
{
  redirfd -w 1 /run/service/.s6-svscan/finish
  heredoc 0 "#!/bin/execlineb -P\ns6-svc -X -- \"/run/service/s6-svscan-log\"\n"
  cat
}
if { cd /run/service/.s6-svscan chmod 0755 crash finish }


# Directory for the catch-all logger to store its logs

if { mkdir -p -m 2700 /run/uncaught-logs }
if { chown catchlog:catchlog /run/uncaught-logs }
if { chmod 2700 /run/uncaught-logs }


# Copy all the early services and link them into the scandir

if { /bin/cp -a /etc/s6/early-services /run/services }
if
{
  forbacktickx -pnd"\n" i { ls -1 -U /run/services }
    importas -u i i
    ln -nsf ../services/$i /run/service/$i
}


# Also link the early services defined in sysinit, if any

if
{
  if -t { test -d /run/early-services }
  forbacktickx -pnd"\n" i { ls -1 -U /run/early-services }
    importas -u i i
    ln -nsf ../early-services/$i /run/service/$i
}


# Create the catch-all logger servicedir directly in the scandir
# (It's too fundamental to be made configurable as an early service.)

if { rm -f /run/service/s6-svscan-log/fifo }
if { mkfifo -m 0600 /run/service/s6-svscan-log/fifo }
if
{
  redirfd -w 1 /run/service/s6-svscan-log/run
  heredoc 0 "#!/bin/execlineb -P
redirfd -w 2 /dev/console
redirfd -rnb 0 fifo
s6-setuidgid catchlog
exec -c
s6-log t /run/uncaught-logs\n"
  cat
}
if { chmod 0755 /run/service/s6-svscan-log/run }


# All ready, exec into s6-svscan.
# By default all messages from all services will go to the catch-all logger it spawns.

redirfd -r 0 /dev/null
redirfd -wnb 1 /run/service/s6-svscan-log/fifo
fdmove -c 2 1
s6-svscan -St0 /run/service
