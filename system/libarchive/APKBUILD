# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libarchive
pkgver=3.3.3
pkgrel=0
pkgdesc="Library for creating and reading streaming archives"
url="https://libarchive.org/"
arch="all"
options="!check"  # needs EUC-JP and KOI8R support in iconv
license="BSD-2-Clause AND BSD-3-Clause AND Public-Domain"
makedepends="zlib-dev bzip2-dev xz-dev lz4-dev acl-dev openssl-dev expat-dev
	attr-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-tools"
source="https://www.libarchive.org/downloads/$pkgname-$pkgver.tar.gz
	seek-error.patch"
builddir="$srcdir/$pkgname-$pkgver"

# secfixes:
#   3.3.2-r1:
#     - CVE-2017-14166

build () {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--with-expat \
		--without-xml2 \
		--with-bz2lib \
		--with-zlib \
		--with-lzma \
		--with-lz4 \
		--enable-acl \
		--enable-xattr \
		ac_cv_header_linux_fiemap_h=no
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

tools() {
	pkgdesc="libarchive tools - bsdtar and bsdcpio"

	mkdir -p "$subpkgdir"/usr/
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
	ln -s bsdtar "$subpkgdir"/usr/bin/tar
	ln -s bsdcpio "$subpkgdir"/usr/bin/cpio
}

sha512sums="9d12b47d6976efa9f98e62c25d8b85fd745d4e9ca7b7e6d36bfe095dfe5c4db017d4e785d110f3758f5938dad6f1a1b009267fd7e82cb7212e93e1aea237bab7  libarchive-3.3.3.tar.gz
ff2567f243ba7e9ce20bc4f7fa422a922c5c23049004efdd8f71f29f93ab9be9aadd4c100e8c6dca318442d583fbad9bd6466017a23f83af18b9808c718b9fce  seek-error.patch"
