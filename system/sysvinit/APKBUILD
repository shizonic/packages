# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=sysvinit
pkgver=2.88
pkgrel=5
pkgdesc="System V-style init programs"
url="https://savannah.nongnu.org/projects/sysvinit"
arch="all"
license="GPL-2.0+"
depends="s6"
makedepends="linux-headers utmps-dev"
install="sysvinit.post-upgrade"
options="!check"
provides="/sbin/init"
subpackages="$pkgname-doc"
source="https://download.savannah.nongnu.org/releases/sysvinit/sysvinit-${pkgver}dsf.tar.bz2
	inittab-2.88
	sysvinit-2.88-posix-header.patch
	utmpx.patch
	"
builddir="$srcdir/sysvinit-${pkgver}dsf"

prepare() {
	cd "$builddir"
	default_prepare

	# util-linux
	sed -i -r \
		-e '/^(USR)?S?BIN/s:\<(last|lastb|mesg)\>::g' \
		-e '/^MAN[18]/s:\<(last|lastb|mesg)[.][18]\>::g' \
		src/Makefile

	# broken
	sed -i -r \
		-e '/^USRBIN/s:utmpdump::g' \
		-e '/^MAN1/s:utmpdump\.1::g' \
		src/Makefile

	# procps
	sed -i -r \
		-e '/\/bin\/pidof/d'\
		-e '/^MAN8/s:\<pidof.8\>::g' \
		src/Makefile
}

build() {
	cd "$builddir"
	export DISTRO="Adélie"
	make -C src
}

package() {
	cd "$builddir"
	make -C src install ROOT="$pkgdir"
	rm "$pkgdir"/usr/bin/lastb || true
	install -D -m644 "$srcdir"/inittab-2.88 "$pkgdir"/etc/inittab
}

sha512sums="0bd8eeb124e84fdfa8e621b05f796804ee69a9076b65f5115826bfa814ac1d5d28d31a5c22ebe77c86a93b2288edf4891adc0afaecc4de656c4ecda8a83807bf  sysvinit-2.88dsf.tar.bz2
52d301225bf0cb0c4d124d205cf3a1e1f4eca21a69179da4882359f79606562354fddf887da8203129bede1d073efa9cdd8fe84a327b51229248b22240b9d5dd  inittab-2.88
27dfe089660a291cbcba06d8564bad11f7fd7c96629e72c2b005562689dc7d8bb479c760e980590906e98423b991ae0acd048713d3bc372174d55ed894abeb3f  sysvinit-2.88-posix-header.patch
3605f88ac3faf7d12bf2269ca5d8625850d53e8583b573ab280fa17066c8e4e5217a0d17b94e47ea67a153ad3b88b433471a77544bd085f01f7d9d353ac16aae  utmpx.patch"
