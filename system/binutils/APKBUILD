# Maintainer: Adelie Platform Group <adelie-devel@lists.adelielinux.org>
pkgname=binutils
pkgver=2.32
pkgrel=1
pkgdesc="Tools necessary to build programs"
url="https://www.gnu.org/software/binutils/"
depends=""
makedepends_build="byacc flex texinfo"
makedepends_host="zlib-dev"
makedepends="$makedepends_build $makedepends_host"
checkdepends="dejagnu"
arch="all"
license="GPL-2.0+ AND GPL-3.0+ AND LGPL-2.0+ AND LGPL-2.1+ AND LGPL-3.0+ AND BSD-3-Clause"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-libs"
# non-PIC is unsupported by musl/ppc
[ "${CARCH}" != "ppc" ] || options='!check'
source="https://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.xz
	binutils-ld-fix-static-linking.patch
	disable-gnu-mbind.patch
	disable-ifunc-tests.patch
	disable-preinit-array-tests.patch
	remove-pr2404-tests.patch
	remove-pr19719-test.patch
	remove-pr19553c-test.patch
	srec.patch
	"

if [ "$CHOST" != "$CTARGET" ]; then
	pkgname="$pkgname-$CTARGET_ARCH"
	subpackages=""
	options="!check"
	sonameprefix="$pkgname:"
	builddir="$srcdir"/binutils-$pkgver
fi

# secfixes:
#   2.28-r1:
#     - CVE-2017-7614
#   2.31.1-r2:
#     - CVE-2018-19931
#     - CVE-2018-19932

build() {
	local _sysroot=/
	local _cross_configure="--enable-install-libiberty"
	local _arch_configure=""

	if [ "$CHOST" != "$CTARGET" ]; then
		_sysroot="$CBUILDROOT"
		_cross_configure="--disable-install-libiberty"
	fi

	# -z separate-code is currently causing kernel miscompiles on x86*
	case "$CTARGET_ARCH" in
	x86 | pmmx | i528) _arch_configure="--disable-separate-code" ;;
	x86_64) _arch_configure="--disable-separate-code --enable-targets=x86_64-pep" ;;
	esac

	case "$CTARGET_ARCH" in
	mips*)  _hash_style_configure="--enable-default-hash-style=sysv" ;;
	*)      _hash_style_configure="--enable-default-hash-style=gnu"	;;
	esac

	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--target=$CTARGET \
		--with-build-sysroot="$CBUILDROOT" \
		--with-sysroot=$_sysroot \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-multilib \
		--enable-shared \
		--enable-ld=default \
		--enable-64-bit-bfd \
		--enable-plugins \
		--enable-relro \
		--disable-gold \
		--enable-deterministic-archives \
		$_cross_configure \
		$_arch_configure \
		$_hash_style_configure \
		--disable-werror \
		--with-system-zlib \
		--enable-lto
	make
}

package() {
	cd "$builddir"
	make install DESTDIR="$pkgdir"
	if [ -d "$pkgdir"/usr/lib64 ]; then
		die "Something really bad happened."
		mv "$pkgdir"/usr/lib64/* "$pkgdir"/usr/lib/
		rmdir "$pkgdir"/usr/lib64
	fi
	if [ "$CHOST" != "$CTARGET" ]; then
		# creating cross tools: remove any files that would conflict
		# with the native tools, or other cross tools
		rm -r "$pkgdir"/usr/share
		rm -f "$pkgdir"/usr/lib/libiberty.a
	fi
}

check() {
	cd "$builddir"
	make check
}

libs() {
	pkgdesc="Runtime libraries from binutils"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/lib*.so "$subpkgdir"/usr/lib/
}

sha512sums="d326408f12a03d9a61a9de56584c2af12f81c2e50d2d7e835d51565df8314df01575724afa1e43bd0db45cfc9916b41519b67dfce03232aa4978704492a6994a  binutils-2.32.tar.xz
ecee33b0e435aa704af1c334e560f201638ff79e199aa11ed78a72f7c9b46f85fbb227af5748e735fd681d1965fcc42ac81b0c8824e540430ce0c706c81e8b49  binutils-ld-fix-static-linking.patch
d378fdf1964f8f2bd0b1e62827ac5884bdf943aa435ec89c29fc84bb045d406b733fffaff8fdd8bd1cba8ddea7701c4cf6ccf3ed76a8a3df9c72b447737575a6  disable-gnu-mbind.patch
474ab24097bbb5b24433620549e5234fe65c547824c1342f693c718ffbc81e2d968259cce2d650b55200dd1ec89da207ea2db10c551cd9941285c4600b4297b2  disable-ifunc-tests.patch
3537752e63cef0b5ef136d003ff7e814ba66b12624d817430112d0f291a792e8960fa69a78036f526af835441b3ee483d6a53d55c7b3dd8ee96f0399682dbcbe  disable-preinit-array-tests.patch
32ab4215669c728648179c124632467573a3d4675e79f0f0d221c22eb2ec1ca5488b79910bd09142f90a1e0d0b81d99ca4846297f4f9561f158db63745facb66  remove-pr2404-tests.patch
a193d1fa7f42d91915960460a15e4d24e0df529d81e23014bcf45d283fae76bb7b300fdcb0d0a9d521cdb9137322efa1dc357112596d6ae7a7fd05988ac359b9  remove-pr19719-test.patch
39ef9c76dd5db6b15f11ffa8061f7ca844fb79c3fb9879c3b1466eef332a28b833597c87003ab9f260b1b85023fae264659088aee27cad7e5aa77b2d58b9a3f6  remove-pr19553c-test.patch
f720b3356b88e366c52941da056e543e4b42bc77f012e5b0290f79e15b0a31d855989ad01920680507a9df0544e5b8e26d0cf8d6f22fbdeb874af31cff4c16d3  srec.patch"
