# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=strace
pkgver=4.26
pkgrel=0
pkgdesc="A useful diagnositic, instructional, and debugging tool"
url="https://strace.io/"
arch="all"
options="!check"  # test suite requires mpers, which requires multilib
license="LGPL-2.1+"
depends=""
makedepends="linux-headers autoconf automake"
subpackages="$pkgname-doc"
source="https://github.com/strace/strace/releases/download/v4.26/strace-4.26.tar.xz
	disable-fortify.patch
	fix-ppc-pt-regs-collision.patch
	nlattr-fix.patch
	"

build() {
	case "$CLIBC" in
	musl) export CFLAGS="$CFLAGS -Dsigcontext_struct=sigcontext" ;;
	esac

	case "$CARCH" in
	s390x)
		# __SIGNAL_FRAMESIZE is defined in asm/sigcontext.h
		# but including it would make conflict with struct sigcontext
		# since we compile with it in musl.
		# Temporarily add this until musl upstream has a proper fix
		# for struct sigcontext.
		export CFLAGS="$CFLAGS -D__SIGNAL_FRAMESIZE=160"
		;;
	esac

	ac_cv_have_long_long_off_t=yes \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-mpers
	make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
}

sha512sums="cce153246802fcdb1547bc11164784066101a2e672e2a264cb742593b559e579aa4bedc749d87fdd1fc2045f4f79b54d419bb50c823074d2cca6f3c75ccf2290  strace-4.26.tar.xz
273b92ebf0069f19bef7ec26c7860e2af7ef01e782255c70ded1ae5e967f8f6bf031ecba96612c6083bf58f46278ba4ab3ec0fb35b08c8c8d668191f97adee52  disable-fortify.patch
b70cee89dd49a2b5a69dc2a56c3a11169d3306e1a73981155188b574486965c034aa52b4ac1c6edff5ef55c9d52f27750acb242fac095a8a9f69689b51b3fad1  fix-ppc-pt-regs-collision.patch
44b1872cf996caa4970fa6c2875a3a2cffe4a38455e328d968bd7855ef9a05cf41190794dc137bc8667576635f5271057cf0e6cde9a6c7aee66afd1dba9bdba0  nlattr-fix.patch"
