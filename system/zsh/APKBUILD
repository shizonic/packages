# Contributor: <kalonji@gmail.com>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=zsh
pkgver=5.7.1
pkgrel=0
pkgdesc="A very advanced and programmable command interpreter (shell)"
url="https://www.zsh.org/"
arch="all"
license="Zsh AND BSD-3-Clause AND GPL-2.0+ AND GPL-2.0-only AND Artistic-1.0-Perl AND MIT AND GPL-1.0+"
depends="debianutils"  # add-shell
makedepends_host="ncurses-dev pcre-dev utmps-dev"
install="zsh.post-install zsh.post-upgrade zsh.pre-deinstall"
# Note the custom fetch() function
source="https://www.zsh.org/pub/$pkgname-$pkgver.tar.xz
	zprofile
	zshrc
	"
subpackages="$pkgname-doc
	     $pkgname-calendar::noarch
	     $pkgname-vcs::noarch
	     $pkgname-tcp::noarch"
[ "$CBUILD" != "$CHOST" ] || subpackages="$subpackages $pkgname-zftp"

# secfixes:
#   5.6.2-r0:
#     - CVE-2018-0502
#     - CVE-2018-13259
#   5.4.1-r1:
#     - CVE-2018-1083
#     - CVE-2018-1100

_libdir="usr/lib/zsh/$pkgver"
_sharedir="usr/share/zsh/$pkgver"

# Move some bigger completion files to subpackages.
_comps="android-tools:Unix/_adb
	bzr:Unix/_bzr
	cvs:Unix/_cvs
	gcc:Unix/_gcc
	git:Unix/_git
	graphicsmagick:Unix/_graphicsmagick
	imagemagick:Unix/_imagemagick
	lynx:Unix/_lynx
	mercurial:Unix/_hg
	rsync:Unix/_rsync
	subversion:Unix/_subversion
	tmux:Unix/_tmux
	zfs:Unix/_zfs*:Unix/_zpool"
for _i in $_comps; do
	subpackages="$subpackages ${_i%%:*}-zsh-completion:_completion:noarch"
done

fetch() {
	uri_fetch_mirror "https://www.zsh.org/pub/old/$pkgname-$pkgver.tar.xz" || true
	default_fetch
}

prepare() {
	default_prepare

	# Remove completions for other systems.
	cd Completion
	rm -Rf AIX BSD Cygwin Darwin Debian Mandriva Redhat Solaris openSUSE

	# Remove completions for programs that are not available on Alpine
	# (just to decrease size of the package).
	cd Unix/Command
	rm -f _aap _apm _baz _bittorrent _bpython _ccal _cdcd _chkconfig _clay \
		_cowsay _cplay _cssh _darcs _devtodo _dict _dsh _elfdump _elm \
		_enscript _finger _flasher _fsh _gnupod _guilt _initctl \
		_mencal _module _monotone _moosic _mysqldiff _nkf \
		_pack _perforce _pine _pkgadd _pkginfo _pkgrm _prcs \
		_quilt _raggle _rcs _rlogin _rubber _sablotron _sisu _socket \
		_stgit _surfraw _tardy _tin _tla _topgit _totd _twidge \
		_unace _unison _units _uzbl _vcsh _vux _wiggle _xmms2 _yodl
	cd ../../Linux/Command
	rm -f _mondo _tpb _tpconfig _uml _vserver
	cd ../../X/Command
	rm -f _acroread _dcop _gnome-gv _gqview _gv _kfmclient _matlab \
		_nautilus _netscape _qiv _vnc _xfig _xloadimage \
		_xournal _xv _xwit
}

build() {
	LIBS="-lutmps -lskarnet" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--bindir=/bin \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--sysconfdir=/etc \
		--enable-etcdir=/etc/zsh \
		--enable-pcre \
		--enable-unicode9 \
		--enable-multibyte \
		--enable-function-subdirs \
		--enable-zsh-secure-free \
		--with-tcsetpgrp
	make
}

check() {
	# Does not work with musl due to UTF-8
	rm "$builddir"/Test/A03quoting.ztst
	# Does not work with musl due to locale
	rm "$builddir"/Test/B03print.ztst
	# Not guaranteed to work portably (requires atime)
	rm "$builddir"/Test/C02cond.ztst
	# PPC?
	rm "$builddir"/Test/V09datetime.ztst
	make test
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm644 "$srcdir"/zprofile "$pkgdir"/etc/zsh/zprofile
	install -Dm644 "$srcdir"/zshrc "$pkgdir"/etc/zsh/zshrc
}

doc() {
	default_doc
	_submv $_sharedir/help
}

calendar() {
	pkgdesc="Calendar Function System for ZSH"
	depends="$pkgname"

	_submv $_sharedir/functions/Calendar
}

vcs() {
	pkgdesc="Version Control Information module for ZSH (vcs_info)"
	depends="$pkgname"

	_submv $_sharedir/functions/VCS_Info
}

tcp() {
	pkgdesc="Network I/O over TCP/IP from within the shell"
	depends="$pkgname"

	_submv $_sharedir/functions/TCP
}

zftp() {
	pkgdesc="Zftp Function System for ZSH"
	depends="$pkgname"

	_submv $_libdir/zsh/zftp.so
	_submv $_sharedir/functions/Zftp
}

_completion() {
	local name="${subpkgname%-zsh-completion}"
	pkgdesc="Zsh completions for $name"
	depends="$pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel $name"

	local files="$(printf '%s\n' $_comps \
		| sed -En "s|^$name:(.*)|\1|p" | tr : ' ')"
	test -n "$files" || { echo "$name not found in \$_comps" >&2; return 1; }

	local f; for f in $files; do
		_submv $_sharedir/functions/Completion/$f
	done
}

_submv() {
	local path="$1"
	mkdir -p "$subpkgdir"/${path%/*}
	mv "$pkgdir"/$path "$subpkgdir"/${path%/*}/
}

sha512sums="a6aa88e1955a80229a4784a128866e325f79a8b5f73c922ab480048411036f1835cbb31daa30ab38bd16ab2a50040eda8f4f1f64704b21b6acc3051b1dbf18d0  zsh-5.7.1.tar.xz
e90723eb790581419498f3ab86fb5d6cab4aaede8c87eb79ead7047d259b643cfb47f4d1beb06d03e038cfda3a20ef9dd4c1b417270ab55c688e166fe30ef4f6  zprofile
1675e016f97333cad89b587f4292d81b6bc10e27b75482e3e0c3808539c95bd49b907b6579fb98d817f77f2f2384c0df5afa3e2b6f43b6ae9b466925cd9ccffc  zshrc"
