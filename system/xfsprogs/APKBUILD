# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=xfsprogs
pkgver=5.0.0
pkgrel=0
pkgdesc="XFS filesystem utilities"
url="http://xfs.org/index.php/Main_Page"
arch="all"
options="!check"  # No test suite.
license="GPL-1.0-only"
makedepends="attr-dev bash icu-dev libedit-dev linux-headers util-linux-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-libs"
source="https://www.kernel.org/pub/linux/utils/fs/xfs/$pkgname/$pkgname-$pkgver.tar.gz
	fix-mmap.patch
	no-utmp-header.patch
	"

build() {
	export DEBUG=-DNDEBUG
	export OPTIMIZER="$CFLAGS"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sbindir=/sbin \
		--libexecdir=/usr/lib \
		--enable-editline
	make SHELL=/bin/bash
}

check() {
	make check
}

package() {
	make -j1 DIST_ROOT="$pkgdir" install install-dev
	find "$pkgdir" -name *.la -delete
	chown -R 0:0 "$pkgdir"
}

sha512sums="9430eeb0231be88fa71040ed38d26a0ae29afdd7ff44a72b758ce45f70eba3bde9b688f1a3bbe91798af0d07d4cd21af7f8f0f2a4f87a874a54465f1b358e764  xfsprogs-5.0.0.tar.gz
c23d5dca744c4589ede517701fc8ea02f9b7a59568d907269048741806d2e6c9e56ed3493163d63dbf16193ff99471206548b25efcda18e3e5dff14eb38066d4  fix-mmap.patch
272bd64cf6aa3311edfea94c15167313a805bab3a659c231142a02cb7fd87b97c811ba68b1ef77bb5737b2e37defe6a41b234143b0951b13c251abab7186645b  no-utmp-header.patch"
