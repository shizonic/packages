# Contributor: William Pitcock <nenolod@dereferenced.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=linux-pam
pkgver=1.3.1
pkgrel=2
pkgdesc="Pluggable Authentication Modules"
url="https://www.kernel.org/pub/linux/libs/pam"
arch="all"
license="BSD-3-Clause"
depends_dev="gettext-tiny-dev"
makedepends_host="$depends_dev utmps-dev"
makedepends_build="$depends_dev byacc flex-dev autoconf automake libtool"
makedepends="$makedepends_host $makedepends_build"
options="suid"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://github.com/$pkgname/$pkgname/releases/download/v1.3.1/Linux-PAM-$pkgver.tar.xz
	fix-compat.patch
	libpam-fix-build-with-eglibc-2.16.patch
	musl-fix-pam_exec.patch
	use-utmpx.patch

	base-auth.pamd
	base-account.pamd
	base-password.pamd
	base-session.pamd
	base-session-noninteractive.pamd
	other.pamd
	su.pamd
	"

builddir="$srcdir"/Linux-PAM-$pkgver
prepare() {
	cd "$builddir"
	default_prepare
	# disable insecure modules
	sed -e 's/pam_rhosts//g' -i modules/Makefile.am
}

build() {
	cd "$builddir"
	autoreconf -vif
	[ "$CLIBC" = "musl" ] && export ac_cv_search_crypt=no
	LIBS="-lutmps -lskarnet" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--libdir=/lib \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--disable-db
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install

	# do not install pam.d files bundled with the source, they could be broken
	rm -rf "$pkgdir"/etc/pam.d

	# install our pam.d files
	mkdir "$pkgdir"/etc/pam.d
	for i in $source; do
		case $i in
		*.pamd)
			basename=$(echo $i | cut -d. -f1)
			cp "$srcdir"/$i "$pkgdir"/etc/pam.d/"$basename"
			# ensure users can use PAM services without SUID
			chmod 644 "$pkgdir"/etc/pam.d/"$basename"
			;;
		esac
	done

	chgrp shadow "$pkgdir"/sbin/unix_chkpwd \
		&& chmod g+s "$pkgdir"/sbin/unix_chkpwd
}

sha512sums="6bc8e2a5b64686f0a23846221c5228c88418ba485b17c53b3a12f91262b5bb73566d6b6a5daa1f63bbae54310aee918b987e44a72ce809b4e7c668f0fadfe08e  Linux-PAM-1.3.1.tar.xz
1a8ae3a212684b0dfef12aaeb1d4cec6e85b056a79b13ddc9ebe1cd61fe8da1a6210a06eb3e4f5bacc0c2fa71b82346d87f673921ce1453cbc46cb905e86a6c6  fix-compat.patch
f49edf3876cc6bcb87bbea4e7beaeb0a382d596898c755f5fbaf6c2ed4e0c8f082b2cd16dde8a74af82bb09a1334f463e07a4bb5b8a48f023ff90a67ad2fdd44  libpam-fix-build-with-eglibc-2.16.patch
82fb1ec27b370ed5d30451f31aecbacf94ff8aff9db52e79090466dcdd1b1b2c18ca7e0641b1b51a3ed78ea7203fe9464b50f63d6dbf661e10f68366c79196ae  musl-fix-pam_exec.patch
8352c0bd36f776251143d1e73d92a1e746e8f23778462e441cc989afd4204887aca6b310d87ab8e5b315b13c4ad1225c87531b71a0fef693772fc7e12bcde418  use-utmpx.patch
2df1d45af0f32ed3755fde2771129f73f28761e0c5d8b08ca880a0206c6eaa3a32cc1bcf27045b960f33d062cff901220acd535e319ae3c4368614dada08cd2a  base-auth.pamd
62144e8f785ce324771465017a27b9a538856ba120d80d1181f5b1012d56170b712c4cd9d018ee51af387a2cdf0442c14f7d07d556abcb2e2bea54bca2c4c262  base-account.pamd
b8e6f5cf4ada79470be9f24cd414dd1bb7918ad2c973d2e19134e27016596142d32b593fff0b9f15b58dc2e9af52763070fe11667815e649c09aef5580f5bc95  base-password.pamd
59b746dbd220ccf7217f5dc01c8c2554bb18a37b48f966b63dcb189e07a19ab0b0187511fed232f26f326d734ee32fa7fd47e0194d6ebd4bed5766247165d553  base-session.pamd
2d42a0a8781a71405ca4512bb32c409ac73cbed0fc4d0bf9483f7825feae0976fd04ef2002f0a8fe4b9ff69a6b98dae060685b4da47769b09b6020a1e5ff0ef0  base-session-noninteractive.pamd
862df6a009dea562e46242552fdbcfa8bc0ebc8abbaa9cf91eae106f9e41557209dfa98cc49968fed05ca9427cb5748ff158433e3502cf80729b050e85cbd60c  other.pamd
1676ee7a95041a3a9c3e3ae03bd714d72b9a47759c1b6c28511071c949df828e5f22814f3751ae4e01bb6dab4444369eeadd3d6e57a0ac8996901e6f0be97296  su.pamd"
