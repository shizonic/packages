# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=oxygen
pkgver=5.12.7
pkgrel=0
pkgdesc="'Oxygen' theme for KDE"
url="https://www.kde.org/"
arch="all"
license="MIT AND (GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1-only AND GPL-2.0+ AND (LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev libxcb-dev
	frameworkintegration-dev kcmutils-dev kcompletion-dev kconfig-dev
	kdecoration-dev kguiaddons-dev ki18n-dev kservice-dev
	kwidgetsaddons-dev kwindowsystem-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/oxygen-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="20e448f223c13f60b929d3fe0be4070868ddf9189a1108b2a798ce08ca654db082798c6389e6dbd06dcc72fff061e3178c805c9f1fb595b1649235e0ec5231ff  oxygen-5.12.7.tar.xz"
