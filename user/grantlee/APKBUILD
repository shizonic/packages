# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=grantlee
pkgver=5.1.0
pkgrel=1
pkgdesc="Libraries for text templating with Qt"
url="https://github.com/steveire/grantlee/"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="cmake $depends_dev qt5-qtscript-dev doxygen graphviz"
subpackages="$pkgname-dev $pkgname-doc"
source="grantlee-$pkgver.tar.gz::https://github.com/steveire/grantlee/archive/v$pkgver.tar.gz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make all docs
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E buildertest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install

	mkdir -p "$pkgdir"/usr/share/doc/grantlee
	mv "$builddir"/apidox "$pkgdir"/usr/share/doc/grantlee/
}

sha512sums="8a4906979f160abf21481d364f0318789c8b5340c62bff06cc62e6714dbf2c52cbfe577a24490052ac25d94f4cc59e11764204236bc7532bd7f1fd5188baa41a  grantlee-5.1.0.tar.gz"
