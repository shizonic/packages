# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcolorchooser
pkgver=19.04.2
pkgrel=0
pkgdesc="Simple application to choose a colour from the screen"
url="https://www.kde.org/applications/graphics/kcolorchooser/"
arch="all"
license="MIT"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev ki18n-dev kxmlgui-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kcolorchooser-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="14aa1edc8852544ce4415fde03ef00191127d1c2b55fda61ad20e1e9c41a43bb276ea720b8d6a77f513decbdbefc31c2eabbc8c66bea5e985cec948ff71cdd90  kcolorchooser-19.04.2.tar.xz"
