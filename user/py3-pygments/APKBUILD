# Contributor: Peter Bui <pnutzh4x0r@gmail.com>
# Contributor: Matt Smith <mcs@darkregion.net>
# Maintainer: zlg <zlg+adelie@zlg.space>
pkgname=py3-pygments
_pkgname=Pygments
pkgver=2.2.0
pkgrel=0
pkgdesc="A syntax highlighting package written in Python."
url="http://pygments.org/"
arch="noarch"
options="!check" # needs nose, flake8
license="BSD-2-Clause"
depends="python3"
makedepends="python3-dev"
subpackages=""
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	cd "$builddir"
	python3 setup.py build
}

# Reactivate once nose and flake8 are packaged
check() {
	cd "$builddir"
	sed -i -e 's:python:python3:' Makefile
	make test
}

package() {
	cd "$builddir"
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

doc() {
	cd "$builddir"
	install -m 644 -D doc/pygmentize.1 "$pkgdir"/usr/share/man/man1/pygmentize.1
}

sha512sums="cc0a4f73e19fa6cbf46314de2e809460c807c631e39ba05cbe5edb5f40db1a687aafcd9715585a0ed45f791710eb6038305e273f282f8682df76f30e63710b29  Pygments-2.2.0.tar.gz"
