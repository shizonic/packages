# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kwin
pkgver=5.12.7
pkgrel=0
pkgdesc="Modern, stylish window manager (requires OpenGL)"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires accelerated X11 desktop
license="GPL-2.0+ AND (GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1-only"
depends="qt5-qtmultimedia"
depends_dev="qt5-qtbase-dev libepoxy-dev libxcb-dev kconfig-dev	kcoreaddons-dev
	kwindowsystem-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev python3
	qt5-qtdeclarative-dev qt5-qtscript-dev qt5-qtsensors-dev eudev-dev
	qt5-qtmultimedia-dev qt5-qtx11extras-dev fontconfig-dev freetype-dev
	libdrm-dev libinput-dev libx11-dev libxkbcommon-dev mesa-dev
	wayland-dev xcb-util-cursor-dev xcb-util-image-dev xcb-util-wm-dev

	breeze-dev kactivities-dev kcompletion-dev kconfigwidgets-dev
	kcmutils-dev kcrash-dev kdeclarative-dev kdecoration-dev
	kglobalaccel-dev ki18n-dev kiconthemes-dev kidletime-dev kinit-dev
	kio-dev knewstuff-dev knotifications-dev kpackage-dev kscreenlocker-dev
	kservice-dev ktextwidgets-dev kwayland-dev kwidgetsaddons-dev
	kxmlgui-dev plasma-framework-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/kwin-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="acb58cd5681727aa2ebcfb75d2abc3ea636811b490d8b15a4b89e55cfb81ce8aeed9568a52d4d1c3274852f26823ec5301a6721d9be64ba9a2b65e04f6029760  kwin-5.12.7.tar.xz"
