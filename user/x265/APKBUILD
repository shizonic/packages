# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=x265
pkgver=3.0
pkgrel=0
pkgdesc="Open Source H265/HEVC video encoder"
url="http://x265.org"
arch="all"
options="textrels"
license="GPL-2.0-only"
makedepends="cmake yasm" # 2.6+ will use nasm instead of yasm
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://bitbucket.org/multicoreware/$pkgname/downloads/${pkgname}_${pkgver}.tar.gz
	x265-unbreak-my-arm.patch"
builddir="$srcdir/${pkgname}_$pkgver"

build() {
	cd "$builddir"/build/linux

	local cmake_opts=""
	case "$CARCH" in
		# unfortunately, AltiVec code is very broken (more than 1000 errors)
		ppc*) cmake_opts="-DENABLE_ALTIVEC=OFF -DCPU_POWER8=OFF";;
	esac

	cmake \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_VERBOSE_MAKEFILE=ON \
		$cmake_opts \
		../../source
	make
}

check() {
	cd "$builddir"/build/linux

	# LEAVE THIS.
	# every breakage so far has been SIGILL, which IS detected by this.
	# LEAVE IT!
	./x265 --version
}

package() {
	cd "$builddir"/build/linux

	make DESTDIR="$pkgdir" install
}

sha512sums="bb7665194ddd4bccbb91c16337463ad4ef32111a1e7779e4cc16964df0992aa99e578cb74fef0edbf41119105e4085574247f60541b0558cc36730ea12d2c6ba  x265-3.0.tar.gz
ac3ecc613fe4da11672de8f397b5541fe2af29b7dd91a48859bfe8824934a5966c41ca30de242564c46f1a0cdd2a0da9331d9f547fc21120066fb3c45347c4c4  x265-unbreak-my-arm.patch"
