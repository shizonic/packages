# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=rocs
pkgver=19.04.2
pkgrel=0
pkgdesc="Graph theory IDE"
url="https://www.kde.org/applications/education/rocs/"
arch="all"
options="!check"  # All tests require X11.
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kdoctools-dev boost-dev
	grantlee-dev qt5-qtwebkit-dev qt5-qtsvg-dev qt5-qtxmlpatterns-dev
	karchive-dev kconfig-dev kcoreaddons-dev kcrash-dev kdeclarative-dev
	ki18n-dev kitemviews-dev ktexteditor-dev kxmlgui-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/rocs-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="d763877e05686fdd15b6481d079808ecb20c0b27b5e90bb8e9a5bf93e878914f3091f51ce3b99895c633124d03cc0110db7231c0e1e5f54f8b792c532dfdde0d  rocs-19.04.2.tar.xz"
