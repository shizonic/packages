# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=i3wm
pkgver=4.15
pkgrel=0
pkgdesc="Improved dynamic tiling window manager"
url="https://i3wm.org"
arch="all"
license="BSD-3-Clause"
options="!check"  # The test suite requires X
makedepends="bison flex libxcb-dev xcb-util-cursor-dev xcb-util-keysyms-dev
	xcb-util-wm-dev libev-dev pango-dev cairo-dev yajl-dev
	startup-notification-dev pcre-dev libxkbcommon-dev xcb-util-xrm-dev"
checkdepends="perl-x11-xcb perl-anyevent perl-json-xs perl-ipc-run
	perl-inline-c perl-dev libxcb-dev xcb-util-dev xorg-server-xephyr"
subpackages="$pkgname-doc"
source="https://i3wm.org/downloads/i3-$pkgver.tar.bz2
	i3wm-musl-glob-tilde.patch
	i3wm-test-fix-off_t.patch
	i3wm-test-disable-branch-check.patch"
builddir="$srcdir/i3-$pkgver"

build() {
	cd "$builddir"
	./configure \
		--prefix="/usr" \
		--disable-builddir \
		--sysconfdir="/etc"
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir/" install

	install -d "$pkgdir/usr/share/man/man1"
	install -m644 man/*.1 "$pkgdir"/usr/share/man/man1/
}

sha512sums="60ab61b7e380342126bea12fb4371f98fcf18f6435f79a9519d3f59cfabdb170634366036e1aa20c5592da0832b03140ad1f0c72bad3cfaace0b7c57ad01dfc4  i3-4.15.tar.bz2
8ce7d00371c43b93dabbe0dadf9caf7c58a68f4a0079f5a9b9552c15c55bfa0df16d7e87a281595af2ac5254632ba28ccf82a467cea16159b41490f6f2910299  i3wm-musl-glob-tilde.patch
77224b994397b2e2487ae28dfd5781b3630654191813eb3c685f05ebf446e65c36e53a665ff3cc8323ea67e87f7cf977044025dade0a6ed22cbd84f0e6b4cbc7  i3wm-test-fix-off_t.patch
a80384965dff62c51ce77e2baa3cf1b0b6db1df68994ce98383f96554bd296b4b59527fb5b1cb24b08c123699e294ba9b3baaa52afe88d87e7a76f0629194b1f  i3wm-test-disable-branch-check.patch"
