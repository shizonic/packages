# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kiriki
pkgver=19.04.2
pkgrel=0
pkgdesc="Yahtzee-like dice game for one or more players"
url="https://games.kde.org/game.php?game=kiriki"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kcoreaddons-dev
	kconfig-dev kcrash-dev kdbusaddons-dev kdoctools-dev ki18n-dev
	kconfigwidgets-dev kiconthemes-dev kxmlgui-dev libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kiriki-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="19200d05a7179dc521ae9bf4e80c65deee04287edc766543abbc3715fe3754dbe295ae4cfbcca61c1fa333591916565e5258987476492b80255d6b51d16f6c55  kiriki-19.04.2.tar.xz"
