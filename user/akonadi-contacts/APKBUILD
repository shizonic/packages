# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=akonadi-contacts
pkgver=18.12.3
pkgrel=1
pkgdesc="Library for integrating contact lists with Akonadi"
url="https://www.kde.org/"
arch="all"
options="!check"  # Tests require X11
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev akonadi-dev kcontacts-dev kcoreaddons-dev
	kservice-dev"
makedepends="$depends_dev cmake extra-cmake-modules akonadi-mime-dev kio-dev
	kmime-dev prison-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/akonadi-contacts-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="dba40ec470d8b34b38d69470128708580a0581dcf1a88191da1b6bfedb426a13c964117eea1d266d1cb2224006a4b26f1f8ccc39e79eac67653aa808c5d81fb0  akonadi-contacts-18.12.3.tar.xz"
