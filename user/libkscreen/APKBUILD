# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libkscreen
pkgver=5.12.7
pkgrel=1
pkgdesc="KDE Plasma screen management software"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires dbus-x11 and both of them running
license="LGPL-2.1+ AND GPL-2.0+ AND (GPL-2.0-only OR GPL-3.0-only)"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qtx11extras-dev
	kwayland-dev"
subpackages="kscreen-doctor:doctor $pkgname-dev $pkgname-wayland"
source="https://download.kde.org/stable/plasma/$pkgver/libkscreen-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

doctor() {
	pkgdesc="KDE Plasma screen debugging and management tool"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

wayland() {
	pkgdesc="$pkgdesc (Wayland support)"
	install_if="$pkgname=$pkgver-r$pkgrel wayland"
	mkdir -p "$subpkgdir"/usr/lib/qt5/plugins/kf5/kscreen
	mv "$pkgdir"/usr/lib/qt5/plugins/kf5/kscreen/KSC_KWayland.so \
		"$subpkgdir"/usr/lib/qt5/plugins/kf5/kscreen/
}

sha512sums="fb24d0f4f79468d4246e912f4b7e814df398911cdbd475e18263e89502ecca35c9e4974b7f0af92afa8cba3081056f8f27b0f051eb287c682dd37b35568b3c5c  libkscreen-5.12.7.tar.xz"
