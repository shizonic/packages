# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=spectacle
pkgver=19.04.2
pkgrel=0
pkgdesc="Application for capturing desktop screenshots"
url="https://www.kde.org/applications/graphics/spectacle/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	kcoreaddons-dev kwidgetsaddons-dev kdbusaddons-dev knotifications-dev
	kconfig-dev ki18n-dev kio-dev kxmlgui-dev kwindowsystem-dev python3
	kdoctools-dev kdeclarative-dev xcb-util-image-dev xcb-util-cursor-dev
	libxcb-dev xcb-util-renderutil-dev knewstuff-dev libkipi-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/spectacle-$pkgver.tar.xz
	qt5.9.patch
	"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="26c1323c57034c45a49637904fa3228f24a6ce1fa1d3dd1512e09e42b5762bdc234092fe955d430f8326212f7032aa16f4a5964a51a0ab724408c6fdce0183a1  spectacle-19.04.2.tar.xz
781e1611026a45232fa117b90a1520bed380e512ae2e0690785a960f897342284ce0c553d8fb22caf10fd34c3823ee7dacb770815c871808345c1bb05126b74f  qt5.9.patch"
