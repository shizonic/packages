# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=valgrind
pkgver=3.15.0
pkgrel=0
pkgdesc="A tool to help find memory-management problems in programs"
url="http://valgrind.org/"
arch="all"
license="GPL-2.0+"
makedepends="autoconf automake libtool bash perl cmd:which"
# from README_PACKAGERS:
#   Don't strip the debug info off lib/valgrind/$platform/vgpreload*.so
#   in the installation tree.  Either Valgrind won't work at all, or it
#   will still work if you do, but will generate less helpful error
#   messages.
options="!strip !check"
subpackages="$pkgname-dev $pkgname-doc"
source="ftp://sourceware.org/pub/$pkgname/$pkgname-$pkgver.tar.bz2
	0001-Ensure-ELFv2-is-supported-on-PPC64.patch
	arm.patch
	realloc.patch
	suppressions.patch
	uclibc.patch
	"

prepare() {
	default_prepare
	cd "$builddir"
	aclocal && autoconf && automake --add-missing
	echo '#include <linux/a.out.h>' > include/a.out.h
}

build() {
	cd "$builddir"
	# fails to build with ccache
	export CC="gcc"
	export CFLAGS="$CFLAGS -fno-stack-protector -no-pie"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--without-mpicc
	make
}

check() {
	cd "$buildir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install

	# we have options=!strip above so we strip the /usr/bin/* manually
	if [ -z "$DEBUG" ]; then
		strip "$pkgdir"/usr/bin/valgrind \
			"$pkgdir"/usr/bin/valgrind-di-server \
			"$pkgdir"/usr/bin/vgdb \
			"$pkgdir"/usr/bin/valgrind-listener \
			"$pkgdir"/usr/bin/cg_merge
	fi
}

sha512sums="5695d1355226fb63b0c80809ed43bb077b6eed4d427792d9d7ed944c38b557a84fe3c783517b921e32f161228e10e4625bea0550faa4685872bb4454450cfa7f  valgrind-3.15.0.tar.bz2
250177c3f50940560de9ad885cf5d9d7aa060ac832ced04875e1109b9114ec34e358a5c2309c8548f657e293cf50d980306d47b2cdf5e429aeabd10ffe666104  0001-Ensure-ELFv2-is-supported-on-PPC64.patch
9ee297d1b2b86891584443ad0caadc4977e1447979611ccf1cc55dbee61911b0b063bc4ad936d86c451cedae410cb3219b5a088b2ad0aa17df182d564fe36cfe  arm.patch
57086a768f3876b26b0e507bc159a73f0955f03d5af8cc30e21103e348ca67f2e58b5555a5a97f299751c6602692ad43d8346bb68a80917d740fb4d65bba9665  realloc.patch
d011b2769d35ca3206967e6dac9f3fb24a6496e5bbfdc73342382245c89375b09d6948b10ba3cf8aa0050c79c0dd13b75764aec0ec5c6ac479fa930941067747  suppressions.patch
d59a10db9037e120df2ee94a103402ca95a79abee9d8be63e4e1bca29c82dca775cc402a79b854ec11a2160a4d2da202c237369418e221d1925267ea2613fd5d  uclibc.patch"
