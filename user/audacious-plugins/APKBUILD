# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: William Pitcock <nenolod@dereferenced.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=audacious-plugins
pkgver=3.10.1
pkgrel=0
pkgdesc="Playlist-oriented media player (plugins)"
url="https://audacious-media-player.org/"
arch="all"
options="!check"  # No test suite
license="ISC"
depends="audacious"
install_if="audacious=$pkgver"
makedepends="audacious-dev>=${pkgver%.*}
	alsa-lib-dev curl-dev dbus-glib-dev faad2-dev ffmpeg-dev flac-dev
	fluidsynth-dev lame-dev libcddb-dev libcdio-dev libcdio-paranoia-dev
	libcue-dev libguess-dev libmodplug-dev libmms-dev libmowgli-dev
	libnotify-dev libogg-dev libsamplerate-dev libsndfile-dev libvorbis-dev
	libxcomposite-dev libxml2-dev mpg123-dev neon-dev pulseaudio-dev
	qt5-qtmultimedia-dev sdl-dev wavpack-dev"
subpackages="$pkgname-lang"
source="https://distfiles.audacious-media-player.org/$pkgname-$pkgver.tar.bz2"

prepare() {
	default_prepare
	msg "Rebuilding configure..."
	aclocal -I m4 && autoheader && autoconf
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-gtk \
		--enable-qt
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="454e9ce4061e92a0ecda40f562d4cc7453fc0019fd76b25dbe9e319319fa37c22f9785cb29563e8074de8a88e6130106aca1e431790297e1b4636dc974fde565  audacious-plugins-3.10.1.tar.bz2"
