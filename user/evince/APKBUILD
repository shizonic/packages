# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=evince
pkgver=3.32.0
pkgrel=0
pkgdesc="GNOME document viewer"
url="https://wiki.gnome.org/Apps/Evince"
arch="all"
license="GPL-2.0+ AND MIT AND BSD-3-Clause AND LGPL-2.0+ AND X11 AND LGPL-3.0-only AND Public-Domain AND LGPL-2.1+"
depends="gst-plugins-base gst-plugins-good"
makedepends="djvulibre-dev glib-dev gobject-introspection-dev
	gsettings-desktop-schemas-dev gstreamer-dev gst-plugins-base-dev
	gtk+3.0-dev itstool libarchive-dev libexecinfo-dev libgxps-dev
	libsecret-dev libspectre-dev libxml2-dev libxml2-utils poppler-dev
	tiff-dev zlib-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://ftp.gnome.org/pub/gnome/sources/evince/3.32/evince-$pkgver.tar.xz"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--without-gspell \
		--disable-nautilus \
		--enable-introspection \
		--enable-ps

	# our msgfmt(1) can't do --desktop
	cp data/org.gnome.Evince.desktop.in data/org.gnome.Evince.desktop
	cp data/org.gnome.Evince-previewer.desktop.in \
		data/org.gnome.Evince-previewer.desktop
	cp backend/pdf/evince-pdfdocument.metainfo.xml.in \
		backend/pdf/evince-pdfdocument.metainfo.xml
	cp backend/pdf/pdfdocument.evince-backend.desktop.in \
		backend/pdf/pdfdocument.evince-backend
	cp backend/djvu/evince-djvudocument.metainfo.xml.in \
		backend/djvu/evince-djvudocument.metainfo.xml
	cp backend/djvu/djvudocument.evince-backend.desktop.in \
		backend/djvu/djvudocument.evince-backend
	cp backend/tiff/evince-tiffdocument.metainfo.xml.in \
		backend/tiff/evince-tiffdocument.metainfo.xml
	cp backend/tiff/tiffdocument.evince-backend.desktop.in \
		backend/tiff/tiffdocument.evince-backend
	cp backend/comics/evince-comicsdocument.metainfo.xml.in \
		backend/comics/evince-comicsdocument.metainfo.xml
	cp backend/comics/comicsdocument.evince-backend.desktop.in \
		backend/comics/comicsdocument.evince-backend
	cp backend/xps/evince-xpsdocument.metainfo.xml.in \
		backend/xps/evince-xpsdocument.metainfo.xml
	cp backend/xps/xpsdocument.evince-backend.desktop.in \
		backend/xps/xpsdocument.evince-backend
	cp backend/ps/evince-psdocument.metainfo.xml.in \
		backend/ps/evince-psdocument.metainfo.xml
	cp backend/ps/psdocument.evince-backend.desktop.in \
		backend/ps/psdocument.evince-backend
	cp org.gnome.Evince.appdata.xml.in org.gnome.Evince.appdata.xml

	# we now return you to your regularly scheduled build
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="565298a200d9ae2f6b4cb53c3cba0d0d0e4cfbef60e4145bfb9c82a5682947ceb2371e52c27179cd69a238cd387bcfd744d3c55df814b6347f07781aec3ea658  evince-3.32.0.tar.xz"
