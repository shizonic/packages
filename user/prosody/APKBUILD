# Contributor: Mika Havela <mika.havela@gmail.com>
# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=prosody
pkgver=0.11.2
pkgrel=0
pkgdesc="Lua based Jabber/XMPP server"
url="http://prosody.im/"
arch="all"
options="!check" # broken testsuite
license="MIT"
depends="lua-socket lua-expat lua-filesystem lua-sec lua5.3"
makedepends="linux-headers lua5.3-dev libidn-dev openssl-dev"
install="prosody.pre-install"
subpackages="$pkgname-doc $pkgname-openrc"
pkgusers="prosody"
pkggroups="prosody"
source="https://prosody.im/downloads/source/$pkgname-$pkgver.tar.gz
	prosody.cfg.lua.patch
	$pkgname.initd
	"

build() {
	cd "$builddir"
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc/prosody \
		--ostype=linux \
		--with-lua-lib=/usr/lib \
		--with-lua-include=/usr/include \
		--lua-version=5.3 \
		--no-example-certs
	# Don't generate certs
	rm -f "$builddir"/certs/Makefile

	make
}

check() {
	cd "$builddir"
	make test
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install

	install -d -o prosody -g prosody "$pkgdir/var/log/prosody"
	install -d -o prosody -g prosody "$pkgdir/var/run/prosody"
	install -d -m750 -o prosody -g prosody "$pkgdir/var/lib/prosody"

	install -D -m755 "$srcdir"/"$pkgname".initd "$pkgdir"/etc/init.d/"$pkgname"
}

sha512sums="22e55a551a40c92f38a1ed1de5fdcad574d699d58c2e27f93c3d465b55487b8e923c2ba088daf93c7140cbafb0d429687e0b662c8bb1c1aba4b79ac1ea271cd1  prosody-0.11.2.tar.gz
a6ca168fe3d11ee3b05295fb36dfaf8240c60a85507032b2502f9a97d3fd055f7eee38ba6efbb8f79472fc7cdd3556922194d0bd7099f7fb809be01890acc511  prosody.cfg.lua.patch
24360603dbd5d2a92758e6c4b4aab4f02cbd05373580cba2df76df98b6045891e8108e8c2d16af9508e93968ed5880db952e7a21b2742ebeec6f14b167968c2c  prosody.initd"
