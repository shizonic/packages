# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Maintainer: 
pkgname=libevent
pkgver=2.1.8
pkgrel=4
pkgdesc="An event notification library"
url="http://libevent.org/"
arch="all"
license="BSD-3-Clause"
depends=""
depends_dev="python3"
makedepends="$depends_dev openssl-dev"
subpackages="$pkgname-dev"
source="https://github.com/$pkgname/$pkgname/releases/download/release-$pkgver-stable/$pkgname-$pkgver-stable.tar.gz
	dont-test-fallback.patch
	fix-test-on-32bit.patch
	py3_dumpevents.patch
	py3_rpcgen.patch
	"

# secfixes:
#   2.1.8-r0:
#   - CVE-2016-10195
#   - CVE-2016-10196
#   - CVE-2016-10197

builddir="$srcdir"/$pkgname-$pkgver-stable

prepare() {
	cd "$builddir"
	default_prepare
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--disable-static
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make -j1 DESTDIR=$pkgdir install
}

dev() {
	replaces="libevent"
	default_dev
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="a2fd3dd111e73634e4aeb1b29d06e420b15c024d7b47778883b5f8a4ff320b5057a8164c6d50b53bd196c79d572ce2639fe6265e03a93304b09c22b41e4c2a17  libevent-2.1.8-stable.tar.gz
d059a592252f83a918f0b6237e2dbee1d05822c83372bcd0f658a25428cce109fd088c5dec8320fef4c1aa7a713ada53aae9b7c04d7ca9b039ed4a483ba84146  dont-test-fallback.patch
7898a00eeab4af7ff3b7c1ee3b90e0d718beba435dfadf015f62084524b2b0f4049c8dd9e16926c61017c01faabf7a51c2f19be7f9532e01278d691acb98465d  fix-test-on-32bit.patch
1f51788db3797870392997d0314fb744ee54d3b1a326d1b67f522fc7af65d50210cb137e8213d35a788bbf3c97aac18cd9860de8af3cb8c82f25e3ae07d662ae  py3_dumpevents.patch
00d0b09425835638a5e29d96d70c855a5c57efb188157b80a3885a2dcbe88709b49ae57aeb6b8b590458a934116cf59934e6e32fbf684b2b3b8333c0dcac837e  py3_rpcgen.patch"
