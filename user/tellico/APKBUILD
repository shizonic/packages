# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=tellico
pkgver=3.1.4
pkgrel=0
pkgdesc="Collection manager"
url="http://tellico-project.org/"
arch="all"
license="GPL-2.0-only OR GPL-3.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev libxml2-dev libxslt-dev
	karchive-dev kcodecs-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev
	kcrash-dev kdoctools-dev kguiaddons-dev khtml-dev ki18n-dev
	kiconthemes-dev kio-dev kitemmodels-dev kjobwidgets-dev kwallet-dev
	kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev solid-dev

	kfilemetadata-dev knewstuff-dev libcdio-dev libksane-dev ncurses-dev
	poppler-dev poppler-qt5-dev taglib-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="http://tellico-project.org/files/tellico-$pkgver.tar.xz
	btparse-strcasecmp.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS -std=gnu99" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	# imagejob: needs running X11
	# htmlexporter: needs plasma desktop
	# filelisting: needs dbus
	# tellicoread: needs network
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(filelisting|imagejob|htmlexporter|tellicoread)test'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="d09183bee13ae9f7c6c5114b76eeeb959a184def559ae239bb880ef9be964e33878530e9c4c0d3baa6f872bf8db155dffb94592e3a857623ab131d2f28f1f139  tellico-3.1.4.tar.xz
4627e717d67340de6d88f7a21604a66ba236c651a0ae38d9d3569b76ad58c79f046cfd5686dd688de86d6acafc17ba3959902babdc7f00ab8e9d65717c4fab4a  btparse-strcasecmp.patch"
