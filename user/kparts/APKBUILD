# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kparts
pkgver=5.54.0
pkgrel=0
pkgdesc="Framework for user interface components"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires X11 running.
license="LGPL-2.1-only AND LGPL-2.1+ AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends=""
depends_dev="qt5-qtbase-dev kio-dev kservice-dev ktextwidgets-dev kxmlgui-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev kconfig-dev kcoreaddons-dev ki18n-dev kiconthemes-dev
	kjobwidgets-dev kwidgetsaddons-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kparts-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="607a9a89b9d3b4434a46fe84fd13588bb2d99a43b6cc7a85abc1c336613834743f0b6ef832f31126cbf3a0ea6260c8e889927d7cc9d7cb0ad61215d1c6c4e272  kparts-5.54.0.tar.xz"
