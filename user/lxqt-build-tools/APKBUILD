# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=lxqt-build-tools
pkgver=0.6.0
pkgrel=0
pkgdesc="Toolchain for building and packaging LXQt"
url="https://lxqt.org"
arch="noarch"
# no tests to run; this package just provides a common base for scaffolding.
options="!check"
license="BSD-3-Clause"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev"
source="lxqt-build-tools-$pkgver.tar.gz::https://github.com/lxde/lxqt-build-tools/archive/$pkgver.tar.gz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	mkdir -p build && cd build
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} ..
	make
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="701b2f239daeb72dd94e2d72920e85e161daeb35de24023d81bb271c3abf36916d83b4b47b046e400e865e71ee236cbfc0a7047f9b0a3529cd9b8363279fb88a  lxqt-build-tools-0.6.0.tar.gz"
