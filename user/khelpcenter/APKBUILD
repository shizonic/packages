# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=khelpcenter
pkgver=19.04.2
pkgrel=0
pkgdesc="Graphical documentation viewer"
url="https://www.kde.org/applications/system/khelpcenter/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev karchive-dev kconfig-dev
	kbookmarks-dev kcoreaddons-dev kdbusaddons-dev kdoctools-dev ki18n-dev
	kinit-dev khtml-dev kservice-dev kwindowsystem-dev grantlee-dev
	xapian-core-dev libxml2-dev"
subpackages="$pkgname-doc $pkgname-lang"
install_if="plasma-desktop docs"
source="https://download.kde.org/stable/applications/$pkgver/src/khelpcenter-$pkgver.tar.xz
	es-doc-fix.patch
	"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="57b0267febb8ae8d6caa29be423a446267c32506bf4c8cfbe11069aad80c581cb66f8d647493edc723905b5db033992e56d85f425f27e388c97f4e8835688836  khelpcenter-19.04.2.tar.xz
4d7e286b7130547d7f26e11a0e5a40d20530e08cf657ff24d31cbc7b110628f564e513e5922a5cffc82d0830731ab03b07820884542292b3c03c7519617a20a3  es-doc-fix.patch"
