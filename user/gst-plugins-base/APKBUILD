# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gst-plugins-base
pkgver=1.16.0
pkgrel=0
pkgdesc="GStreamer multimedia framework - Base plugins"
url="https://gstreamer.freedesktop.org/"
arch="all"
options="!check"  # fails overlaycomposition on ppc64
license="GPL LGPL"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
depends=""
replaces="gst-plugins-base1"
makedepends="alsa-lib-dev cdparanoia-dev expat-dev glib-dev
	gobject-introspection-dev gstreamer-dev libice-dev libogg-dev libsm-dev
	libtheora-dev libvorbis-dev libx11-dev libxt-dev libxv-dev mesa-dev
	opus-dev orc-dev pango-dev perl cmd:which !gst-plugins-base"
checkdepends="orc-compiler"
source="https://gstreamer.freedesktop.org/src/gst-plugins-base/gst-plugins-base-$pkgver.tar.xz
	endian.patch
	"
ldpath="/usr/lib/gstreamer-1.0"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--disable-static \
		--disable-experimental \
		--disable-fatal-warnings \
		--with-default-audiosink=alsasink \
		--enable-introspection \
		--with-package-name="GStreamer Base Plugins (${DISTRO_NAME:-Adélie Linux})" \
		--with-package-origin="${DISTRO_URL:-https://www.adelielinux.org/}"
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make -j1 DESTDIR="$pkgdir" install
}

doc() {
	default_doc
	replaces="${pkgname}1-doc"
}

sha512sums="4fa404156a384000e26e72ff0f701edb5a160181b48362350529177ca1c9325a3c58b83904bf5056262fe2609bc7f5913e835278973e049315deecfd1f26a3af  gst-plugins-base-1.16.0.tar.xz
f146e76bedabd7b028cc64a9a671f7fb1ac1655164159d47953ea1afddafee1e40470593442f22152fb7e3579847dc4fc6e1f6e16a7c47252d5f3c25a6ae7353  endian.patch"
