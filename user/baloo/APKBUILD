# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=baloo
pkgver=5.54.0
pkgrel=0
pkgdesc="Semantic desktop search framework"
url="https://www.kde.org/"
arch="all"
# Test #4 in the suite, bin/querytest:
# Test cleanup method is not run after testTermEqual.
# This leaves a `db` handle stale, which is using the single-reader-per-thread
# pthread_key that lmdb provides.
# This causes the next test, testTermStartsWith, to fail to acquire a reader
# transaction because the previous transaction was not committed or aborted.
# Since the test does not check for a transaction failure and blindly uses the
# transaction object returned, it attempts to access 0xc (accessing a member of
# the returned nullptr), and segfaults.
# Three other tests in the suite fail in the same way.
# This may be a bug in libQtTest, or in musl.  I have not debugged further.
# This has already taken up two days of my time and I just want to ship Plasma
options="!check"
license="(LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.1+"
depends=""
depends_dev="kcoreaddons-dev kfilemetadata-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qtbase-dev qt5-qttools-dev kconfig-dev kcrash-dev kdbusaddons-dev
	ki18n-dev kidletime-dev kio-dev lmdb-dev solid-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/baloo-$pkgver.tar.xz
	initialise-variable.patch"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="ec610aa2ab6409cf206623e08eb78d7ccc3b278ca94d6f8ba79d6d705459eb5cb9d74ed9ffc4cc20c337582b12517fb59da1570aa689111c0f105746234333de  baloo-5.54.0.tar.xz
5622f1eb7a692c0ebd5d904b421612292375d3ea3f4ffdc4624fa8e2dc4a5fe56e5636f11a388224642be03c8fc28a678cffb9b167eb3de48ff4ae2979e6435e  initialise-variable.patch"
