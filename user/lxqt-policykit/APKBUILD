# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=lxqt-policykit
pkgver=0.14.1
pkgrel=0
pkgdesc="PolicyKit agent for LXQT"
url="https://lxqt.org"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qttools-dev polkit-qt-1-dev
	liblxqt-dev>=${pkgver%.*}.0 lxqt-build-tools>=0.6.0 qt5-qttools-dev
	kwindowsystem-dev"
subpackages="$pkgname-doc"
source="https://github.com/lxqt/lxqt-policykit/releases/download/$pkgver/lxqt-policykit-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	mkdir -p build && cd build
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} ..
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="c3df41a45ef4d04eba9ce0c9f29d4c9b68a192302e85e192a26ae73617b87d0ad70ec64135f500485d4c10095754e36935bf9790ffd83d0197b1bb784971a940  lxqt-policykit-0.14.1.tar.xz"
