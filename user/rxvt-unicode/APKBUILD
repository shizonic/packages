# Contributor: Moritz Wilhelmy
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Jakub Skrzypnik <j.skrzypnik@openmailbox.org>
# Contributor: William Pitcock <nenolod@dereferenced.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=rxvt-unicode
pkgver=9.22
pkgrel=4
pkgdesc="Fork of the rxvt terminal emulator with improved unicode support"
url="http://software.schmorp.de/pkg/rxvt-unicode.html"
arch="all"
options="!check"  # No test suite.
license="(GPL-2.0+ OR BSD-2-Clause) AND GPL-2.0+ AND GPL-3.0+"
depends="$pkgname-terminfo"
makedepends="libx11-dev libxft-dev ncurses fontconfig-dev
	gdk-pixbuf-dev libxrender-dev perl-dev startup-notification-dev"
subpackages="$pkgname-doc $pkgname-terminfo::noarch"
source="http://dist.schmorp.de/$pkgname/$pkgname-$pkgver.tar.bz2
	gentables.patch
	kerning.patch"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--with-terminfo=/usr/share/terminfo \
		--enable-256-color \
		--enable-font-styles \
		--enable-xim \
		--enable-keepscrolling \
		--enable-selectionscrolling \
		--enable-smart-resize \
		--enable-pixbuf \
		--enable-transparency \
		--enable-frills \
		--enable-perl \
		--enable-mousewheel \
		--enable-text-blink \
		--enable-fading \
		--enable-startup-notification \
		--enable-unicode3 \
		--disable-utmp \
		--disable-wtmp \
		--disable-lastlog
	make
}

package() {
	# despite having a separate terminfo subpackage
	# TERMINFO env var is used by rxvt-unicode makefile
	# leaving it as is ~skrzyp
	export TERMINFO="$pkgdir/usr/share/terminfo"
	mkdir -p "$TERMINFO"

	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

terminfo() {
	pkgdesc="$pkgdesc (terminfo data)"
	depends=""

	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/terminfo "$subpkgdir"/usr/share/terminfo
}

sha512sums="b39f1b2cbe6dd3fbd2a0ad6a9d391a2b6f49d7c5e67bc65fe44a9c86937f8db379572c67564c6e21ff6e09b447cdfd4e540544e486179e94da0e0db679c04dd9  rxvt-unicode-9.22.tar.bz2
a45074b8fe39ffb712bd53b03521a8611fe5887a97ea09c1e85a7086de1042dd0360269803ffe5fcc56425af3c0cc3a55c214b2ef0fcfa2c3a298b4b37d261cb  gentables.patch
42314393f7f061f1aa2cf2fedd3d84e96d3104868b0629cefd9e9b313529afde52127a412992e76935fa2de8d4e685d6b5ce42162cb8d1b0365de63d10c11925  kerning.patch"
