# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=xorg-server
pkgver=1.20.4
pkgrel=0
pkgdesc="X.Org X11 server"
url="https://www.X.Org/"
arch="all"
license="MIT"
options="suid"
subpackages="$pkgname-dbg $pkgname-dev $pkgname-doc xvfb $pkgname-xephyr
	$pkgname-xnest"
# the modesetting driver is now shipped with xorg server
replaces="xf86-video-modesetting"
depends="font-cursor-misc font-misc-misc xkeyboard-config xkbcomp xinit"
depends_dev="libepoxy-dev libxfont2-dev mesa-dev"
makedepends="$depends_dev autoconf automake libtool util-macros
	eudev-dev libpciaccess-dev libdrm-dev libepoxy-dev pixman-dev
	libx11-dev libxdamage-dev libxinerama-dev libxkbfile-dev libxkbui-dev
	libxv-dev libxxf86dga-dev libxxf86misc-dev xcb-util-dev
	xcb-util-image-dev xcb-util-keysyms-dev xcb-util-renderutil-dev
	xcb-util-wm-dev xorgproto-dev
	xtrans
	openssl-dev perl zlib-dev
	"
source="https://www.X.Org/releases/individual/xserver/$pkgname-$pkgver.tar.bz2
	autoconfig-sis.patch
	fix-musl-arm.patch
	"

# secfixes:
#   1.20.3-r0:
#     - CVE-2018-14665
#   1.19.5-r0:
#     - CVE-2017-12176
#     - CVE-2017-12177
#     - CVE-2017-12178
#     - CVE-2017-12179
#     - CVE-2017-12180
#     - CVE-2017-12181
#     - CVE-2017-12182
#     - CVE-2017-12183
#     - CVE-2017-12184
#     - CVE-2017-12185
#     - CVE-2017-12186
#     - CVE-2017-12187
#     - CVE-2017-13721
#     - CVE-2017-13723

prepare() {
	default_prepare

	# Fix dbus config path
	sed -i -e 's/\$(sysconfdir)/\/etc/' config/Makefile.*
	sed -i -e 's/termio.h/termios.h/' hw/xfree86/os-support/xf86_OSlib.h
}

build() {
	export CFLAGS="$CFLAGS -D_GNU_SOURCE"
	[ "$CLIBC" == musl ] && export CFLAGS="$CFLAGS -D__gid_t=gid_t -D__uid_t=uid_t"
	export LDFLAGS="$LDFLAGS -Wl,-z,lazy"

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc/X11 \
		--localstatedir=/var \
		--with-xkb-path=/usr/share/X11/xkb \
		--with-xkb-output=/var/lib/xkb \
		--without-systemd-daemon \
		--enable-composite \
		--enable-config-udev \
		--enable-dri \
		--enable-dri2 \
		--enable-glamor \
		--enable-ipv6 \
		--enable-kdrive \
		--enable-xace \
		--enable-xcsecurity \
		--enable-xephyr \
		--enable-xnest \
		--enable-xorg \
		--enable-xres \
		--enable-xv \
		--disable-xwayland \
		--disable-config-hal \
		--disable-dmx \
		--disable-systemd-logind \
		--with-os-vendor="${DISTRO_NAME:-Adélie Linux}"

	make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
	chmod u+s "$pkgdir"/usr/bin/Xorg

	# Don't conflict with xf86-input-evdev
	rm -f "$pkgdir"/usr/share/X11/xorg.conf.d/10-evdev.conf

	install -m755 -d "$pkgdir"/etc/X11/xorg.conf.d
	install -m755 -d "$pkgdir"/var/lib/xkb
	install -m644 -D COPYING "$pkgdir"/usr/share/licenses/$pkgname/COPYING
}

xvfb() {
	pkgdesc="X.Org server for virtual framebuffer (for testing)"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/Xvfb "$subpkgdir"/usr/bin/
}

xephyr() {
	pkgdesc="kdrive-based X11 server (windowed framebuffer on X11 server)"
	depends=
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/Xephyr "$subpkgdir"/usr/bin/
}

xnest() {
	pkgdesc="X.Org nested server"
	depends=
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/Xnest "$subpkgdir"/usr/bin/
}

xwayland() {
	pkgdesc="X.Org server for Wayland"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/Xwayland "$subpkgdir"/usr/bin/
}

sha512sums="f1c92ef6d7613e0636973d3395b48dfdad42230847ab1c8b1cea84647a968f649f9aba97bdb01c10ee8351cbe954d4e6ca4a0fc84bb8fa662d49c8ba2aee00a8  xorg-server-1.20.4.tar.bz2
30a78f4278edd535c45ee3f80933427cb029a13abaa4b041f816515fdd8f64f00b9c6aef50d4eba2aaf0d4f333e730399864fd97fa18891273601c77a6637200  autoconfig-sis.patch
b799e757a22a61ac283adbd7a8df1ad4eccce0bb6cac38a0c962ba8438bba3cf6637a65bb64859e7b32399fca672283a49960207e186c271ba574580de360d09  fix-musl-arm.patch"
