# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=lxqt-globalkeys
pkgver=0.14.1
pkgrel=0
pkgdesc="Daemon and configuration utility for global shortcuts in LXQt"
url="https://lxqt.org"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules lxqt-build-tools>=0.6.0
	liblxqt-dev>=${pkgver%.*}.0 qt5-qttools-dev kwindowsystem-dev"
subpackages="$pkgname-dev"
source="https://github.com/lxqt/lxqt-globalkeys/releases/download/$pkgver/lxqt-globalkeys-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	mkdir -p build && cd build
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} ..
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="7683971a916adb0dff8efd5567dad11e0860e48960bb6ed650aa2cd8e1bc33f7b8cbff95e19f198e00b61eea4f7492b41e7a0acf19365143c1c0ce8ea0cbb2a3  lxqt-globalkeys-0.14.1.tar.xz"
