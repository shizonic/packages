# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: William Pitcock <nenolod@dereferenced.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=audacious
pkgver=3.10.1
pkgrel=0
pkgdesc="A playlist-oriented media player with multiple interfaces"
url="https://audacious-media-player.org/"
arch="all"
options="!check"  # No test suite.
license="ISC"
depends_dev="qt5-qtbase-dev dbus-glib-dev"
makedepends="$depends_dev libguess-dev>=1.2 libxml2-dev autoconf automake"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://distfiles.audacious-media-player.org/$pkgname-$pkgver.tar.bz2"

prepare() {
	default_prepare
	msg "Rebuilding configure..."
	aclocal -I m4 && autoheader && autoconf
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--enable-qt \
		--disable-gtk
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="e88891caaa3897f9b4abf39136e20834aedf1287d0d5eefea392fda89050db8db00c6f363976a68fe250ddbae4e27590f7615916a76370a44ca9235f1fa60b43  audacious-3.10.1.tar.bz2"
