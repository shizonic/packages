# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=rdesktop
pkgver=1.8.4
pkgrel=1
pkgdesc="Remote Desktop Protocol client"
url="https://www.rdesktop.org/"
arch="all"
options="!check"  # No test suite.
license="GPL-3.0-only"
depends=""
makedepends="alsa-lib-dev krb5-dev libice-dev libsamplerate-dev libx11-dev
	libxrandr-dev openssl-dev"
subpackages="$pkgname-doc"
source="https://github.com/rdesktop/rdesktop/releases/download/v$pkgver/rdesktop-$pkgver.tar.gz
	gssapi.patch
	signed-int.patch
	"

prepare() {
	cd "$builddir"
	default_prepare
	update_config_sub
	./bootstrap
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-smartcard
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="9e4f6723eb0baab31ad11f1c5c29a4753c655386c2381d01646b7834c959ffc2ec1e0c2f3f73626255aa018889709758d97387c7563da98bb1b7f269610929ae  rdesktop-1.8.4.tar.gz
ae91f4e0eb1e2c3141aedef660eb89628b334155c8c0559805cc4ac75274fff94101daf665052b27fe5074ce7468ff854fab865e7efd377d08d525319c7aa150  gssapi.patch
e8b4af70a54944d83b7c899aa680042f559e75af3e9a3deb2c7395f8b4a56e50d1c2f26bd10b2377ff577115d635c2aa0fdbddf995588f1d492badfc3e72456e  signed-int.patch"
