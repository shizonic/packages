# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=labplot
pkgver=2.6
pkgrel=0
pkgdesc="Interactive tool for graphing and analysis of scientific data"
url="https://www.kde.org/applications/education/labplot/"
arch="all"
license="GPL-2.0-only"
depends="shared-mime-info"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	karchive-dev kcompletion-dev kconfigwidgets-dev kcoreaddons-dev kio-dev
	kdoctools-dev ki18n-dev kiconthemes-dev kdelibs4support-dev kxmlgui-dev
	knewstuff-dev ktextwidgets-dev kwidgetsaddons-dev gsl-dev fftw-dev
	qt5-qtserialport-dev syntax-highlighting-dev bison libexecinfo-dev
	cantor-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/labplot/$pkgver.0/labplot-$pkgver.0.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=Debug \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS -D_GNU_SOURCE" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	# gives incorrect results
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E fittest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="6ebc96b3cfb25ebd62a6631d8cca0d9574f84548c04271eb8a10ff3ef9ebbdd0bf842b9b6129a995953412c5e6f902440dc91dbaa01d97fc220d7f6b19dc46af  labplot-2.6.0.tar.xz"
