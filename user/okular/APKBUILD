# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=okular
pkgver=19.04.2
pkgrel=0
pkgdesc="Universal document reader developed by KDE"
url="https://okular.kde.org/"
arch="all"
license="GPL-2.0+ AND LGPL-2.0+"
depends="kirigami2"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	qt5-qtdeclarative-dev karchive-dev kbookmarks-dev kcompletion-dev
	kconfigwidgets-dev kcoreaddons-dev kdoctools-dev kiconthemes-dev
	kio-dev kjs-dev kparts-dev kwallet-dev kwindowsystem-dev khtml-dev
	threadweaver-dev kactivities-dev poppler-qt5-dev tiff-dev qca-dev
	libjpeg-turbo-dev kpty-dev kirigami2-dev djvulibre-dev libkexiv2-dev
	libspectre-dev ebook-tools-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/okular-$pkgver.tar.xz
	es-doc-fix.patch
	"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS -D_GNU_SOURCE" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	# All other tests require X11
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -R '^shelltest'
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="1bf79a5d72bdfbcf7caa4104b56cc1e5f5b72e0390521d2cac9a86ae8d66d495632a8af203bffb987fc6d40da41af0c54a8ebce3023b486dbc3eebbd35a866f7  okular-19.04.2.tar.xz
d82dd9de666a28ef605d8a81e74851f265be4ccaeaa39a1cdee9eb1db830fcd0d581d01d6e89a1d36b8ea5c9c8113f1016090dc0e9f17070d388166d9e967458  es-doc-fix.patch"
