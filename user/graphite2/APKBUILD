# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: 
pkgname=graphite2
_realname=${pkgname/2/}
pkgver=1.3.13
pkgrel=0
pkgdesc="Text processing engine for complex languages"
url="http://graphite.sil.org/"
arch="all"
options="!check"  # Test suite requires Graphite 1 and py2-fonttools
license="LGPL-2.1+ OR MPL-2.0+ OR GPL-2.0+"
depends=""
depends_dev="freetype-dev"
makedepends="$depends_dev cmake"
install=""
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/silnrsi/$_realname/archive/$pkgver.tar.gz
	graphite2-1.2.0-cmakepath.patch
	"
builddir="$srcdir"/$_realname-$pkgver

build() {
	cd "$builddir"

	# Fix linking error on 32-bit platforms
	sed -i -e 's/\-nodefaultlibs//g' src/CMakeLists.txt

	mkdir build && cd build
	cmake -G "Unix Makefiles" .. \
		-DCMAKE_C_FLAGS:STRING="${CFLAGS}" \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE:STRING=RelWithDebInfo \
		-DGRAPHITE2_COMPARE_RENDERER=OFF \
		-DGRAPHITE2_NFILEFACE=ON \
		-DGRAPHITE2_NSEGCACHE=ON

	# fix unwanted -O3 cflag (taken from Debian)
	find . -type f ! -name "rules" ! -name "changelog" -exec sed -i -e 's/\-O3//g' {} \;
	make
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir/" install
}

sha512sums="8e2c6047a66b4d8873842de6b9b1909d326512b47a04f483b3a6113b97fd0d47e0da7cf9ecf96e060ca06e5288b49f1300e2fd7165d2d82d6a8eb8cb7d53d33f  graphite2-1.3.13.tar.gz
4ef5414e6d554bb8d6ead435e38d061a073f350c313b7141158bb68332f5f57ca5250385875a387b828bb657964588e974143b96b5e11c2cd314871e7baddb88  graphite2-1.2.0-cmakepath.patch"
