# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libksane
pkgver=19.04.2
pkgrel=0
pkgdesc="KDE scanning library"
url="https://www.kde.org"
arch="all"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends=""
depends_dev="sane-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qtbase-dev ki18n-dev
	ktextwidgets-dev kwallet-dev kwidgetsaddons-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/libksane-$pkgver.tar.xz
	frameworks.patch
	"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="8fcf07000356c40fb25a6b1f4bff6b86d93a3ff836dd5d5f09acfb4c18728a4a355ae895220941cd4ee62456187458c1d649342c79ff838f172acc4fb7edfddb  libksane-19.04.2.tar.xz
60e0e8b073499e932d626a65c5cc23f58d2701921da547733d2736b58c6d412c8d6e782884916390ff16e02183a53ad283712f0bcf2cdcf5a0eb0d7029bb1ba8  frameworks.patch"
