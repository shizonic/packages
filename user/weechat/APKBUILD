# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Contributor: zlg <zlg+adelie@zlg.space>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=weechat
pkgver=2.4
pkgrel=0
pkgdesc="Fast, light, extensible ncurses-based chat client"
url="https://www.weechat.org"
arch="all"
options="!check"  # requires all plugins be built.
license="GPL-3.0+"
depends_dev="cmake aspell-dev curl-dev gnutls-dev libgcrypt-dev lua5.3-dev
	ncurses-dev perl-dev python3-dev zlib-dev"
makedepends="$depends_dev"
checkdepends="cpputest"
subpackages="$pkgname-dev $pkgname-aspell:_plugin $pkgname-lua:_plugin
	$pkgname-perl:_plugin $pkgname-python:_plugin $pkgname-lang"
source="https://www.weechat.org/files/src/$pkgname-$pkgver.tar.gz
	libintl-fix.patch
	"

# secfixes:
#   1.7.1-r0:
#   - CVE-2017-8073
#   1.9.1-r0:
#   - CVE-2017-14727

prepare() {
	cd "$builddir"
	default_prepare
}

build() {
	cd "$builddir"
	mkdir -p build
	cd build
	cmake .. -DCMAKE_INSTALL_PREFIX=/usr -DENABLE_MAN=ON -DENABLE_TESTS=ON -DENABLE_PYTHON3=ON
	make
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir/" install
}

_plugin() {
	local _name=${subpkgname#*-}
	local _dir=usr/lib/weechat/plugins
	pkgdesc="WeeChat $_name plugin"
	depends="weechat"
	if [ "$_name" = python ]; then
		depends="$depends python3"
	fi

	mkdir -p "$subpkgdir"/$_dir
	mv "$pkgdir"/$_dir/${_name}.so "$subpkgdir"/$_dir
}

check() {
	cd "$builddir"/build
	ctest -V
}

sha512sums="3537c3032237b0f1d103849516ccb461a29e13bf37f021af7905c75a3dc7c70fa4a76be2e8559b0299165c114d0408f3267fb74eb21a70f1661e6dac35e3bb9a  weechat-2.4.tar.gz
59841bc343b1d10a542631eb01380789f96cac896380dbb3b159444c4806bd6367952e457b9ffd42fb87c1e19fc77eba78c38fd2178ef202ab9f7f1a543417ca  libintl-fix.patch"
