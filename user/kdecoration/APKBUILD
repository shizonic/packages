# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdecoration
pkgver=5.12.7
pkgrel=0
pkgdesc="Window decoration plugin library"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires running X11 display
license="LGPL-2.1"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/plasma/$pkgver/kdecoration-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="acebb17f42e46dfcb57b5f94e2612e0c4f773ad15e911543281c578de4a409b8f65a028b458afd01f578abb69906316b3c6f2b6d6edd4918b302a8a26227ad95  kdecoration-5.12.7.tar.xz"
