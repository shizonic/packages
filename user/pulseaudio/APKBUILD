# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=pulseaudio
pkgver=12.2
pkgrel=3
pkgdesc="A featureful, general-purpose sound server"
url="https://www.freedesktop.org/wiki/Software/PulseAudio"
pkgusers="pulse"
pkggroups="pulse pulse-access"
arch="all"
options="!check"  # Complains about permissions in /var/tmp.
license="LGPL-2.1+"
makedepends="alsa-lib-dev bash bluez-dev check-dev dbus-dev eudev-dev fftw-dev
	gconf-dev gtk+3.0-dev intltool libcap-dev libsndfile-dev m4 openssl-dev
	orc-compiler orc-dev sbc-dev speexdsp-dev cmd:which"
checkdepends="check-dev"
install="$pkgname.pre-install $pkgname.pre-upgrade"
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs $pkgname-bluez
	$pkgname-alsa $pkgname-utils $pkgname-system:system:noarch
	$pkgname-bash-completion:bashcomp:noarch $pkgname-lang
	$pkgname-zsh-completion:zshcomp:noarch"
source="https://freedesktop.org/software/pulseaudio/releases/pulseaudio-$pkgver.tar.xz
	$pkgname.initd
	$pkgname.confd
	disable-flat-volume.patch
	"

build() {
	cd "$builddir"
	LIBS="-lintl" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-bluez5 \
		--disable-bluez4 \
		--enable-nls \
		--enable-orc \
		--enable-largefile \
		--disable-solaris \
		--enable-default-build-tests \
		--enable-udev \
		--enable-ipv6 \
		--enable-gconf \
		--with-fftw
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make -j1 DESTDIR="$pkgdir" install
}

system() {
	depends="$pkgname"
	pkgdesc="PulseAudio system-wide init scripts"
	replaces="$pkgname-openrc"
	install -D -m755 "$srcdir"/$pkgname.initd \
		"$subpkgdir"/etc/init.d/$pkgname
	install -D -m644 "$srcdir"/$pkgname.confd \
		"$subpkgdir"/etc/conf.d/$pkgname
	mv "$pkgdir"/etc/dbus-1 "$subpkgdir"/etc
}

libs() {
	pkgdesc="PulseAudio libraries"
	mkdir -p "$subpkgdir"/usr/lib \
		"$subpkgdir"/etc/pulse
	mv "$pkgdir"/usr/lib/pulseaudio \
		"$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libpulse.so.0* \
		"$pkgdir"/usr/lib/libpulse-simple.so.0* \
		"$pkgdir"/usr/lib/libpulse-mainloop-*.so.0* \
		"$subpkgdir"/usr/lib/
	mv "$pkgdir"/etc/pulse/client.conf \
		"$subpkgdir"/etc/pulse/
}

bluez() {
	pkgdesc="PulseAudio Bluetooth support"
	mkdir -p "$subpkgdir"/usr/lib/pulse-$pkgver/modules
	mv "$pkgdir"/usr/lib/pulse-$pkgver/modules/*bluez*.so \
		"$pkgdir"/usr/lib/pulse-$pkgver/modules/*bluetooth*.so \
		"$subpkgdir"/usr/lib/pulse-$pkgver/modules/
}

alsa() {
	pkgdesc="PulseAudio ALSA support"
	install_if="$pkgname=$pkgver-r$pkgrel alsa-lib"
	mkdir -p "$subpkgdir"/usr/lib/pulse-$pkgver/modules
	mv "$pkgdir"/usr/lib/pulse-$pkgver/modules/*alsa*.so \
		"$subpkgdir"/usr/lib/pulse-$pkgver/modules/
}

utils() {
	pkgdesc="PulseAudio utilities"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/pa* \
		"$pkgdir"/usr/bin/esdcompat \
		"$subpkgdir"/usr/bin/
}

zeroconf() {
	pkgdesc="PulseAudio zeroconf support"
	mkdir -p "$subpkgdir"/usr/lib/pulse-$pkgver/modules
	mv "$pkgdir"/usr/lib/pulse-$pkgver/modules/*avahi*.so \
		"$pkgdir"/usr/lib/pulse-$pkgver/modules/*zeroconf*.so \
		"$pkgdir"/usr/lib/pulse-$pkgver/modules/*raop*.so \
		"$subpkgdir"/usr/lib/pulse-$pkgver/modules/
}

bashcomp() {
	pkgdesc="Bash completion for $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"
	install -dm755 "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/bash-completion "$subpkgdir"/usr/share
}

zshcomp() {
	pkgdesc="Zsh completion for $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel zsh"
	install -dm755 "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/zsh "$subpkgdir"/usr/share
}

sha512sums="877754c1838b3cb042dbc18a5f1cc3cf313ffcaee7a64703330406d1f86279c34f1107634ac3083b158365e6757fbacf5ec406bc3c5788d291de67b77a561a4e  pulseaudio-12.2.tar.xz
34fe54ece5df60ce63a7955cd828a2716670fef71f40960698ae5518fdaf9cd599f4d8f8852e2c88d715600a9ad06a38984415e5eb320071012e5eb6e5c1b8b1  pulseaudio.initd
75b54581591519d63a3362b155c0f9b0501a60763ab394693a456c44d0216138cf3a40bdd0f7442028663bc045e9ffee286f8f8eaf2ee3bb17379b43615fee0e  pulseaudio.confd
dcb50f7c4fd86b0311ab050f7f0340dcf54379a685903951f22e24df6aee5486ee5e02f866c9e53dd55a54ba302658ad282114ce37f169d185855dc37dae0349  disable-flat-volume.patch"
