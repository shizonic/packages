# Contributor: Kiyoshi Aman <kiyoshi.aman@gmail.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=protobuf
_gemname=google-protobuf
pkgver=3.7.1
_tstver=1.8.0
pkgrel=0
pkgdesc="Library for extensible, efficient structure packing"
url="https://github.com/google/protobuf"
arch="all"
options="!check"  # Broken everywhere.
license="BSD-3-Clause"
depends_dev="zlib-dev"
makedepends="$depends_dev autoconf automake libtool ruby ruby-dev ruby-rake
	ruby-rake-compiler python3 python3-dev"
checkdepends="ruby-json ruby-test-unit"
subpackages="ruby-$_gemname:_ruby py3-$pkgname:_python $pkgname-dev $pkgname-vim::noarch"
source="$pkgname-$pkgver.tar.gz::https://github.com/google/$pkgname/archive/v$pkgver.tar.gz
	googletest-$_tstver.tar.gz::https://github.com/google/googletest/archive/release-$_tstver.tar.gz"

prepare() {
	default_prepare

	cd "$builddir"
	./autogen.sh

	# symlink tests to the test directory
	rm -rf third_party/*
	ln -sf "$srcdir"/googletest-release-$_tstver \
		"$builddir"/third_party/googletest
}

build() {
	# Build Protobuf
	cd "$builddir"
	CXXFLAGS="$CXXFLAGS -fno-delete-null-pointer-checks" LDFLAGS="$LDFLAGS -latomic" \
		./configure --prefix=/usr \
			--sysconfdir=/etc \
			--mandir=/usr/share/man \
			--infodir=/usr/share/info \
			--localstatedir=/var
	make

	# Build for Ruby
	cd "$builddir"/ruby
	# Generate proto files for built-in protocols.
	rake genproto
	gem build $_gemname.gemspec
	gem install --local \
		--install-dir dist \
		--ignore-dependencies \
		--no-document \
		--verbose \
		$_gemname

	# Build for Python 3
	cd "$builddir"/python
	export LD_LIBRARY_PATH=${builddir}/src/.libs
	python3 setup.py build --cpp_implementation

	# Build test-suite
	local test; for test in googletest googlemock; do
		cd "$builddir/third_party/googletest/$test"
		autoreconf -vfi
		./configure
		make
	done
}

check() {
	cd "$builddir"
	make check
	cd "$builddir"/ruby
	rake test
	cd "$builddir"/python
	python3 setup.py test --cpp_implementation
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

_ruby() {
	pkgdesc="Ruby bindings to Google's data interchange format"

	local gemdir="$subpkgdir/$(ruby -e 'puts Gem.default_dir')"
	cd "$builddir"/ruby/dist

	mkdir -p "$gemdir"
	cp -r extensions gems specifications "$gemdir"/

	# Remove duplicated .so libs (should be only in extensions directory).
	find "$gemdir"/gems/ -name "*.so" -delete

	# Remove unnecessary files.
	cd "$gemdir"/gems/$_gemname-$pkgver
	rm -r ext/ tests/
}

_python() {
	pkgdesc="Python bindings to Google's data interchange format"

	cd "$builddir"/python
	python3 setup.py install --prefix=/usr --root="$subpkgdir" \
		--cpp_implementation
}

vim() {
	pkgdesc="Vim syntax for $pkgname"
	depends=""
	install_if="$pkgname=$pkgver-r$pkgrel vim"

	install -Dm644 "$builddir"/editors/proto.vim \
		"$subpkgdir"/usr/share/vim/vimfiles/syntax/proto.vim
}

sha512sums="7d4cfabd4bd55926336a3baafa0bc1f1f15380b1b2af945f70a2bb3ba24c6ac6567f49c492326d6d1c43a488166bff178f9266377758a05d8541d8b242f4f80f  protobuf-3.7.1.tar.gz
1dbece324473e53a83a60601b02c92c089f5d314761351974e097b2cf4d24af4296f9eb8653b6b03b1e363d9c5f793897acae1f0c7ac40149216035c4d395d9d  googletest-1.8.0.tar.gz"
