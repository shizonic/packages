# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kate
pkgver=19.04.2
pkgrel=0
pkgdesc="Advanced text editor with autocomplete, syntax highlighting, and more"
url="https://kate-editor.org/"
arch="all"
options="!check"  # All tests require X11.
license="(LGPL-2.1-only OR LGPL-3.0-only) AND GPL-3.0+"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qtbase-dev python3
	qt5-qtscript-dev kconfig-dev kcrash-dev ki18n-dev kjobwidgets-dev
	kio-dev kparts-dev ktexteditor-dev kwindowsystem-dev kxmlgui-dev
	kiconthemes-dev kdoctools-dev kwallet-dev kservice-dev kitemmodels-dev
	knewstuff-dev threadweaver-dev libgit2-dev kactivities-dev
	plasma-framework-dev"
subpackages="kwrite kwrite-doc:kwrite_doc kwrite-lang:kwrite_lang
	$pkgname-doc $pkgname-lang $pkgname-project"
source="https://download.kde.org/stable/applications/$pkgver/src/kate-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

project() {
	pkgdesc="KDE Advanced Text Editor - Plugin for project management"
	mkdir -p "$subpkgdir"/usr/lib/qt5/plugins/ktexteditor/
	mv "$pkgdir"/usr/lib/qt5/plugins/ktexteditor/kateprojectplugin.so \
		"$subpkgdir"/usr/lib/qt5/plugins/ktexteditor/
}

kwrite() {
	pkgdesc="Simple GUI text editor"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/kwrite "$subpkgdir"/usr/bin/

	mkdir -p "$subpkgdir"/usr/share/metainfo
	mv "$pkgdir"/usr/share/metainfo/org.kde.kwrite.appdata.xml \
		"$subpkgdir"/usr/share/metainfo/

	mkdir -p "$subpkgdir"/usr/share/applications
	mv "$pkgdir"/usr/share/applications/org.kde.kwrite.desktop \
		"$subpkgdir"/usr/share/applications/
}

kwrite_doc() {
	local _langpath

	pkgdesc="Simple GUI text editor (documentation)"
	install_if="docs kwrite=$pkgver-r$pkgrel"
	for _langpath in "$pkgdir"/usr/share/doc/HTML/*; do
		local _sublangpath=${_langpath/"$pkgdir"/"$subpkgdir"}
		if [ -d "$_langpath"/kwrite ]; then
			mkdir -p "$_sublangpath"
			mv "$_langpath"/kwrite "$_sublangpath"/
		fi
	done
}

kwrite_lang() {
	local _langpath

	pkgdesc="Simple GUI text editor (translations)"
	for _langpath in "$pkgdir"/usr/share/locale/*/LC_MESSAGES; do
		local _sublangpath=${_langpath/"$pkgdir"/"$subpkgdir"}
		if [ -f "$_langpath"/kwrite.mo ]; then
			mkdir -p "$_sublangpath"
			mv "$_langpath"/kwrite.mo "$_sublangpath"/
		fi
	done
}

sha512sums="568af7df0588d65f386f482f944b44c7bacffeecf374fce4b4e276be8ca556cbf983c2df2e99a3900c15bd7dfaa0d71b6540c37b4c2773a5293b3e5cba1d3939  kate-19.04.2.tar.xz"
