# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gnupg
pkgver=2.2.16
pkgrel=0
pkgdesc="GNU Privacy Guard 2 - PGP replacement"
url="https://www.gnupg.org/"
arch="all"
license="GPL-3.0+ AND GPL-2.0+ AND LGPL-2.1+ AND LGPL-3.0+ AND MIT AND BSD-3-Clause AND Public-Domain"
depends="pinentry"
pkggroups="gnupg"
makedepends="bzip2-dev gnutls-dev libassuan-dev libgcrypt-dev libgpg-error-dev
	libksba-dev libusb-dev npth-dev openldap-dev sqlite-dev zlib-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://gnupg.org/ftp/gcrypt/$pkgname/$pkgname-$pkgver.tar.bz2
	0001-Include-sys-select.h-for-FD_SETSIZE.patch
	fix-i18n.patch
	60-scdaemon.rules"
install="$pkgname.pre-install $pkgname.pre-upgrade"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--enable-nls \
		--enable-bzip2 \
		--enable-tofu \
		--enable-scdaemon \
		--enable-ccid-driver
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	mkdir -p "$pkgdir"/lib/udev/rules.d
	install -Dm644 "$srcdir"/60-scdaemon.rules "$pkgdir"/lib/udev/rules.d
}

sha512sums="0e0040905cc4d1d9d29e184cfeda520b43990e4ec459212537c0ce6092de987157e05b1d1a3022398d9b3cbaeea0f58a7e686745f96933e5ac26be4229162247  gnupg-2.2.16.tar.bz2
c6cc4595081c5b025913fa3ebecf0dff87a84f3c669e3fef106e4fa040f1d4314ee52dd4c0e0002b213034fb0810221cfdd0033eae5349b6e3978f05d08bcac7  0001-Include-sys-select.h-for-FD_SETSIZE.patch
b19a44dacf061dd02b439ab8bd820e3c721aab77168f705f5ce65661f26527b03ea88eec16d78486a633c474120589ec8736692ebff57ab9b95f52f57190ba6b  fix-i18n.patch
4bfb9742279c2d1c872d63cd4bcb01f6a2a13d94618eff954d3a37451fa870a9bb29687330854ee47e8876d6e60dc81cb2569c3931beaefacda33db23c464402  60-scdaemon.rules"
