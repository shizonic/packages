# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gwenview
pkgver=19.04.2
pkgrel=0
pkgdesc="Fast and easy image viewer by KDE"
url="https://www.kde.org/applications/graphics/gwenview/"
arch="all"
options="!check"  # All tests require X11.
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev phonon-dev
	kio-dev kactivities-dev kitemmodels-dev ki18n-dev kdoctools-dev
	kparts-dev kwindowsystem-dev kiconthemes-dev knotifications-dev
	libkipi-dev libjpeg-turbo-dev libpng-dev lcms2-dev zlib-dev exiv2-dev
	baloo-dev libkdcraw-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/gwenview-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="38592c6f456fa9d6627a4044b06da0688126fc2f0cffdd0c9cfcc9c31886981d73d4cc7c4c764385c05c0d1e39155ea2c7ff6c2c1c3e81361a3537d31b80907f  gwenview-19.04.2.tar.xz"
