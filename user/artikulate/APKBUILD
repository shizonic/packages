# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=artikulate
pkgver=19.04.2
pkgrel=0
pkgdesc="Pronunciation trainer for languages"
url="https://www.kde.org/applications/education/artikulate/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtxmlpatterns-dev
	kdoctools-dev ki18n-dev kconfig-dev kcrash-dev knewstuff-dev
	kxmlgui-dev karchive-dev qt5-qtmultimedia-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/artikulate-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	# TestCourseFiles needs X11
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E TestCourseFiles
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="69f6e509c753fd104fb0895728e3b8ee1b5f02b268f161676d2b46146fab9afb49535fcc3aea79f3b2ddf6a3b6ba1e3d672bbb3d0a873ddc42731b0c86977291  artikulate-19.04.2.tar.xz"
