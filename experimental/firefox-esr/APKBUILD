# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=firefox-esr
pkgver=60.7.0
pkgrel=0
pkgdesc="Firefox web browser"
url="https://www.mozilla.org/en-US/firefox/organizations/faq/"
arch="all"
options="!check"  # Tests disabled
license="GPL LGPL MPL"
depends=""
depends_dev="alsa-lib-dev bzip2-dev dbus-glib-dev gconf-dev gtk+2.0-dev
	gtk+3.0-dev hunspell-dev icu-dev libevent-dev libidl-dev
	libjpeg-turbo-dev libnotify-dev libogg-dev libtheora-dev libvorbis-dev
	libxt-dev libxcomposite-dev mesa-dev nspr-dev nss-dev
	pulseaudio-dev sqlite-dev startup-notification-dev wireless-tools-dev"
makedepends="$depends_dev autoconf2.13 automake libtool
	bsd-compat-headers cmd:which nss-static openssl-dev sed yasm zip
	cargo clang llvm8-dev rust"
subpackages=""
_py2ver=2.7.15
source="https://ftp.mozilla.org/pub/firefox/releases/${pkgver}esr/source/firefox-${pkgver}esr.source.tar.xz
	mozconfig
	stab.h

	fix-fortify-inline.patch
	fix-seccomp-bpf.patch
	mallinfo.patch
	musl-pthread-setname.patch

	fix-arm-version-detect.patch
	mozilla-build-arm.patch
	missing-header-s390x.patch

	bad-google-code.patch
	cmsghdr.patch
	disable-gecko-profiler.patch
	endian-gfx.patch
	endianness-js.patch
	fix-bug-1261392.patch
	fix-mutex-build.patch
	fix-sandbox-membarrier.patch
	proper-system-hunspell.patch
	rust-config.patch
	rust-1.33.0.patch
	shut-up-warning.patch
	skia.patch
	stackwalk-x86-ppc.patch
	webrtc-broken.patch

	firefox.desktop
	firefox-safe.desktop

	https://www.python.org/ftp/python/$_py2ver/Python-$_py2ver.tar.xz"

builddir="$srcdir"/firefox-$pkgver
_mozappdir=/usr/lib/firefox
ldpath="$_mozappdir"

unpack() {
	default_unpack
	[ -z $SKIP_PYTHON ] || return 0

	msg "Killing all remaining hope for humanity and building Python 2..."
	cd "$srcdir/Python-$_py2ver"
	[ -d ../python ] && rm -r ../python
	# 19:39 <+solar> just make the firefox build process build its own py2 copy
	# 20:03 <calvin> TheWilfox: there's always violence
	./configure --prefix="$srcdir/python"
	make -j $JOBS
	# 6 tests failed:
	#    test__locale test_os test_posix test_re test_strptime test_time
	# make test
	make -j $JOBS install
}

prepare() {
	cd "$builddir"
	default_prepare
	cp "$srcdir"/stab.h toolkit/crashreporter/google-breakpad/src/
	cp "$srcdir"/mozconfig "$builddir"/mozconfig
	echo "ac_add_options --enable-optimize=\"$CFLAGS\"" >> "$builddir"/mozconfig
	echo "ac_add_options --host=\"$CHOST\"" >> "$builddir"/mozconfig
	echo "ac_add_options --target=\"$CTARGET\"" >> "$builddir"/mozconfig
	# too much memory
	if [ -z "$JOBS" -o $JOBS -gt 32 ]; then
		echo "mk_add_options MOZ_MAKE_FLAGS=\"-j32\"" >> "$builddir"/mozconfig
	fi
}

build() {
	cd "$builddir"

	export SHELL=/bin/sh
	export BUILD_OFFICIAL=1
	export MOZILLA_OFFICIAL=1
	export USE_SHORT_LIBNAME=1
	# gcc 6
	export CXXFLAGS="-fno-delete-null-pointer-checks -fno-schedule-insns2"

	# set rpath so linker finds the libs
	export LDFLAGS="$LDFLAGS -Wl,-rpath,${_mozappdir}"

	local extra_flags=""
	[ "$CARCH" = "s390x" ] && extra_flags="--disable-startupcache"

	export PATH="$srcdir/python/bin:$PATH"
	./mach build
}

package() {
	cd "$builddir"
	export PATH="$srcdir/python/bin:$PATH"
	DESTDIR="$pkgdir" ./mach install

	install -m755 -d ${pkgdir}/usr/share/applications
	install -m755 -d ${pkgdir}/usr/share/pixmaps

	local png
	for png in browser/branding/official/default*.png; do
		local i=${_png%.png}
		i=${i##*/default}
		install -D -m644 "$png" \
			"$pkgdir"/usr/share/icons/hicolor/${i}x${i}/apps/firefox.png
	done

	install -m644 "$builddir"/browser/branding/official/default48.png \
		${pkgdir}/usr/share/pixmaps/firefox.png
	install -m644 ${startdir}/firefox.desktop \
		${pkgdir}/usr/share/applications/firefox.desktop
	install -m644 ${startdir}/firefox-safe.desktop \
		${pkgdir}/usr/share/applications/firefox-safe.desktop

	# install our vendor prefs
	install -d "$pkgdir"/$_mozappdir/browser/defaults/preferences

	cat >> "$pkgdir"/$_mozappdir/browser/defaults/preferences/firefox-branding.js <<- EOF
	// Use LANG environment variable to choose locale
	pref("intl.locale.matchOS", true);

	// Disable default browser checking.
	pref("browser.shell.checkDefaultBrowser", false);

	// Don't disable our bundled extensions in the application directory
	pref("extensions.autoDisableScopes", 11);
	pref("extensions.shownSelectionUI", true);
	EOF
}

sha512sums="c2152857f5f1c816a12fcf5c450268025ee47ee9299ae3355650d86c7c97191b731123a4964154222ca5ba1edc44fee0d1d5f803ae9515841283ecaff6dc9a55  firefox-60.7.0esr.source.tar.xz
76e3e6f64f4dd1206106b2b73cca3ccd312a8d907010c382ab9e2f0a497fed297abf0654d4dd90c563e6dde6403e3f9084ce927bb275fec95be87bed8adf9257  mozconfig
0b3f1e4b9fdc868e4738b5c81fd6c6128ce8885b260affcb9a65ff9d164d7232626ce1291aaea70132b3e3124f5e13fef4d39326b8e7173e362a823722a85127  stab.h
09bc32cf9ee81b9cc6bb58ddbc66e6cc5c344badff8de3435cde5848e5a451e0172153231db85c2385ff05b5d9c20760cb18e4138dfc99060a9e960de2befbd5  fix-fortify-inline.patch
70863b985427b9653ce5e28d6064f078fb6d4ccf43dd1b68e72f97f44868fc0ce063161c39a4e77a0a1a207b7365d5dc7a7ca5e68c726825eba814f2b93e2f5d  fix-seccomp-bpf.patch
bdcd1b402d2ec94957ba5d08cbad7b1a7f59c251c311be9095208491a05abb05a956c79f27908e1f26b54a3679387b2f33a51e945b650671ad85c0a2d59a5a29  mallinfo.patch
3f535b1e4e9a5a0506ef6f6d9427a84fae1efa3c8e8d6b25b21046d2d04c132bde1a7a98e415067e5c542f699d89249b8cc3d925e0be4e96db498037429691ef  musl-pthread-setname.patch
015e1ff6dbf920033982b5df95d869a0b7bf56c6964e45e50649ddf46d1ce09563458e45240c3ecb92808662b1300b67507f7af272ba184835d91068a9e7d5b0  fix-arm-version-detect.patch
e61664bc93eadce5016a06a4d0684b34a05074f1815e88ef2613380d7b369c6fd305fb34f83b5eb18b9e3138273ea8ddcfdcb1084fdcaa922a1e5b30146a3b18  mozilla-build-arm.patch
6beba471b6a7fc229a9c105d58137f80cec07219e52bb9a666c787b37ca88635d6ec2eba651eccb5096429c2965709c9475c89839c255c3545f8c96c5d40f403  missing-header-s390x.patch
ace7492f4fb0523c7340fdc09c831906f74fddad93822aff367135538dacd3f56288b907f5a04f53f94c76e722ba0bab73e28d83ec12d3e672554712e6b08613  bad-google-code.patch
a8f3589dade59f05a78fb92c5a50785ad2eb3f2a49a88dba0198bf74d3dc9a35e5adc554a5a4235b3653602ef58371ab7afc5a0459b6e9d82c351862e1a2db85  cmsghdr.patch
03597515a38f54aa973d2209ca7fbf7348f9843c4dd0481fae747ac06b35fc7e5c61b2e17ab6e3677e479901e8e12c58e961eb1cc861af7cfd2a8488d4dcde72  disable-gecko-profiler.patch
11361b009d708e80afcf2eca8b02381be0c8314421eb439200c674cfa4e4bca15127f2d0dd11bf36f48d0e309d4424f949caf588f08458f8e9ba6812b83023ab  endian-gfx.patch
363f1df1a78799783b4e9f887dc85d83ec83c6ec392aeacbd98dc89e543d0cd3b9d1964ce274142b44847a03c47f845cbf6fa7fa17a04b25bbaa37fc5d714730  endianness-js.patch
a50b412edf9573a0bd04a43578b1c927967a616b73a5995eefb15bfa78fd2bd14e36ec05315a0703f6370ecd524e6bcb012e7285beb1245e9add9b8553acb79e  fix-bug-1261392.patch
d5d3ec190b1916d5c06f466a72124a3d438d90ea40d54e894105a9faf2aa2a6ce9cc21a0a80bd4408f7be867e5a660ee3d8aed40c8d1fb511cfb947739f45240  fix-mutex-build.patch
48d2af2b5e5c2a6e7038f2710d09142da5ae97a54e07b80823c1aba7fc07f367a84d131a1c513c4d9a8be0a56e362a2c86b8547ae138ba71ee218a30ed34d3cc  fix-sandbox-membarrier.patch
1a88c21c39d5878e2018463fe08dd3a960cdf10e650e06ef3e4f089a431800b50267d8749a7acde196bb47c45aa66058a6ed4fb4d60de2ab69e8a48cd5a109bc  proper-system-hunspell.patch
fd3b165e26ab931dd7eaf220d578c30b8772eab0a870710d59403c9823c2233ef941cd7eb25d1625d705de9e8a7138d0e8c4e227a185e9b687553132da96d81a  rust-config.patch
7450eeb7ca48ce632fb7379aec6018012a0db5efd8afaee2708501e6c72273ba238922696da584a73b7fa82dc202470aba9ef5df20c230c9e21087436e68e4b8  rust-1.33.0.patch
21100340ff03f4257fa6a4b59e206d4d1b71c074ebae392ba616f9e6e6d4988a22eec69b40534221f9453ec3a58397fa4ab99b87abdcb7d22fabb13f9cc577d2  shut-up-warning.patch
8ef2bc4404601f66c7505f3e0a2f6d1d11e8f11e5a888dce4942cf27c86fbdbcdd66cb3d98738b7d9e25538793986140e75d51a893d22c70714ed98ef50a9894  skia.patch
e196c998a4029e4fc134fbe4b7455abb4b93644f7f07b88faef4eda900de225e72b124b5a9ab3492cbe398e17f8390d29b769a2544744a106bba6a5def540973  stackwalk-x86-ppc.patch
76409619464259e3ba52e38d640c5b6210a7fecdc75cf124c185ef356507d4d266a845e9fcdeb7766dcd547f70748123c4fa1670f6e52aadd001a3c866dc2d51  webrtc-broken.patch
f3b7c3e804ce04731012a46cb9e9a6b0769e3772aef9c0a4a8c7520b030fdf6cd703d5e9ff49275f14b7d738fe82a0a4fde3bc3219dff7225d5db0e274987454  firefox.desktop
5dcb6288d0444a8a471d669bbaf61cdb1433663eff38b72ee5e980843f5fc07d0d60c91627a2c1159215d0ad77ae3f115dcc5fdfe87e64ca704b641aceaa44ed  firefox-safe.desktop
27ea43eb45fc68f3d2469d5f07636e10801dee11635a430ec8ec922ed790bb426b072da94df885e4dfa1ea8b7a24f2f56dd92f9b0f51e162330f161216bd6de6  Python-2.7.15.tar.xz"
