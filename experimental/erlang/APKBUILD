# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=erlang
pkgver=21.2
pkgrel=0
pkgdesc="Soft real-time system programming language"
url="https://www.erlang.org/"
arch="all"
license="Apache-2.0"
depends=""
makedepends="libxml2-utils libxslt-dev m4 ncurses-dev openssl-dev perl
	unixodbc-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="http://erlang.org/download/otp_src_$pkgver.tar.gz
	fix-wx-linking.patch
	"
builddir="$srcdir/otp_src_$pkgver"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-shared-zlib \
		--enable-ssl=dynamic-ssl-lib \
		--enable-threads
	make
}

check() {
	local _header

	cd "$builddir"
	export ERL_TOP=$builddir

	make release_tests

	for _header in erl_fixed_size_int_types.h \
		${CHOST}/erl_int_sizes_config.h \
		erl_memory_trace_parser.h; do
		cp erts/include/$_header erts/emulator/beam/
	done
	cd release/tests/test_server
	$ERL_TOP/bin/erl -s ts install -s ts smoke_test batch -s init stop
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="5707ef0d49a9af12cd9d93c3e4fd200092773645838bbe0163782d0fa40562d2b48bdb07f0e650311bfec896ed9e3c9621b4d0ba114c578d7ca3209c2a96ac24  otp_src_21.2.tar.gz
5f1b05d8be71d5e3d7e8c5ad019329af8f68174251b5b6e0a9ee9cb3da51a10983b8696e23b3954c19de5d54783ec16f38c80c74724341dbafb22fcac83c77d4  fix-wx-linking.patch"
