# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: 
pkgname=heimdal
pkgver=7.5.0
pkgrel=2
pkgdesc="An implementation of Kerberos 5"
url="http://www.h5l.org/"
arch="all"
options="suid"
license="BSD-3-Clause AND BSD-2-Clause AND Public-Domain AND MIT"
depends="krb5-conf"
depends_dev="openssl-dev e2fsprogs-dev db-dev"
makedepends="$depends_dev autoconf automake bash libtool
	ncurses-dev perl libedit-dev sqlite-dev texinfo perl-json"
subpackages="$pkgname-doc $pkgname-dev $pkgname-libs $pkgname-openrc"
source="https://github.com/heimdal/heimdal/releases/download/heimdal-$pkgver/heimdal-$pkgver.tar.gz
	heimdal-kadmind.initd
	heimdal-kdc.initd
	heimdal-kpasswdd.initd

	005_all_heimdal-suid_fix.patch
	heimdal_missing-include.patch
	only-build-libedit-when-necessary.patch
	"

# secfixes:
#   7.4.0-r2:
#     - CVE-2017-17439
#   7.4.0-r0:
#     - CVE-2017-11103

prepare() {
	[ -e /usr/lib/libasn1.so ] && echo "## remove old heimdal pkg first ##" && return 1

	default_prepare
	sh ./autogen.sh
}

build() {
	export LDFLAGS="${LDFLAGS} -Wl,--as-needed"
	export LIBS="-ldb"

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-shared=yes \
		--without-x \
		--with-berkeley-db \
		--with-libedit=/usr \
		--with-libedit-lib=/usr/lib \
		--with-sqlite3=/usr \
		--with-openssl=/usr

	# make sure we use system version
	rm -r lib/sqlite lib/com_err

	# workarount a parallell build issue
	make -C lib/asn1 der-protos.h der-private.h
	make -C lib/kadm5 kadm5-protos.h kadm5-private.h kadm5_err.h
	make -C lib/krb5 krb5-protos.h krb5-private.h krb5_err.h krb_err.h \
		heim_err.h k524_err.h
	make -C lib/hx509 hx509-private.h  hx509-protos.h
	make
}

check() {
	make -j1 check
}

package() {
	make DESTDIR="$pkgdir" exec_prefix=/usr sysconfdir=/etc \
	mandir=/usr/share/man infodir=/usr/share/info datadir=/var/lib/heimdal \
	localstatedir=/var/lib/heimdal libexecdir=/usr/sbin install


	install -m755 -D "$srcdir"/heimdal-kadmind.initd \
		"$pkgdir"/etc/init.d/heimdal-kadmind
	install -m755 -D "$srcdir"/heimdal-kdc.initd \
		"$pkgdir"/etc/init.d/heimdal-kdc
	install -m755 -D "$srcdir"/heimdal-kpasswdd.initd \
		"$pkgdir"/etc/init.d/heimdal-kpasswdd

	for i in 1 3 5 8; do
		rm -rf "$pkgdir"/usr/share/man/cat$i
	done

	# Remove conflicts
	# e2fsprogs
	rm -f "$pkgdir"/usr/bin/compile_et \
		"$pkgdir"/usr/share/man/man1/compile_et.1

	# Compress info pages
	for page in heimdal hx509; do
		gzip -9 "$pkgdir"/usr/share/info/${page}.info
	done

	# Install the license
	install -d "$pkgdir"/usr/share/licenses/$pkgname
	install -D -m644 "$builddir"/LICENSE \
		"$pkgdir"/usr/share/licenses/$pkgname/
}

libs() {
	pkgdesc="Heimdal libraries"
	replaces="heimdal"
	depends="krb5-conf"
	mkdir -p "$subpkgdir"/usr/bin "$subpkgdir"/usr/sbin
	mv "$pkgdir"/usr/lib "$subpkgdir"/usr/
	mv "$pkgdir"/usr/bin/string2key \
		"$pkgdir"/usr/bin/verify_krb5_conf \
		"$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/sbin/kdigest \
		"$pkgdir"/usr/sbin/digest-service \
		"$subpkgdir"/usr/sbin/

}

sha512sums="6d1ad77e795df786680b5e68e2bfefee27bd0207eab507295d7af7053135de9c9ebb517d2c0235bc3a7d50945e18044515f0d76c0899b6b74aa839f1f3e5b131  heimdal-7.5.0.tar.gz
0ae0fec4bdb3907d9e82e788e12ef185dd00e6db4c17f55758da5600fedd72ed1118b6b492d039f91cc54d54bf2f79f624ea38a68067e424b737b128494a4bbd  heimdal-kadmind.initd
4dca69bb1c1c6dfce8c0fc1da84855e4549be478ab09511fa5143ee61d1609fed7f3303179bc1e499b0f20445e04c41eda132dd1c5f72e2fea4fcf60a35ad2a9  heimdal-kdc.initd
abee8390632fa775e74900d09e5c72b02fe4f9616b43cc8d0a76175486ed6d4707fb3ce4d06ceb09b0e8d1384e037c3cff6525e11def0122c35c32eebd0d196f  heimdal-kpasswdd.initd
2a6b20588a86a9ea3c35209b96ef2da0b39bc3112aec1505e69a60efc9ffb9ddc1d0dbdfaf864142e9d2f81da3d2653de56d6ffa01871c20fde17e4642625c56  005_all_heimdal-suid_fix.patch
e89efdc942c512363aac1d9797c6bf622324e9200e282bc5ed680300b9e1b39a4ea20f059cdac8f22f972eb0af0e625fd41f267ebcafcfec0aaa81192aff79c1  heimdal_missing-include.patch
d1c50b0a656f15afeae78ce0ace0f9adceea028e118f3952a724d23c63bba7d5c9a50980de16c7606a93769c0aa48ce3b932e8a64f5d7a2127d31d2f39e9688d  only-build-libedit-when-necessary.patch"
